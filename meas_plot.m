close all; clear all;
G = 9.81;
MAX_T = 60000;
samples = 0;

%%
%data = readtable('2021-02-07-golf-s.csv');
data = readtable('MMA_meas.csv');
%load data.mat
acc_x = data.x;
acc_y = data.y;
acc_z = data.z;
t = data.timestamp + 1; % matlab iterates from 1
cross = 0;
for i=2:1:length(t)
    if mod(t(i), MAX_T + 1) == 0
        cross = cross + 1;
    end
    if cross > 0 && t(i) < MAX_T
        t(i) = t(i) + MAX_T*cross;
    end
end

%freq = 1/ (15/1000); %freq in Hz calculated by delay in ms 
%freq = 66.6; % Hz
freq = 800;
if (samples == 0)
    time = t/freq;
else
    time = t;
end
dt = time(2) - time(1);

Ex = mean(acc_x);
Vx = var(acc_x);
Ey = mean(acc_y);
Vy = var(acc_y);
Ez = mean(acc_z);
Vz = var(acc_z);

%% Raw acceleration
figure()
plot_them_all(time, acc_x, acc_y, acc_z, 'acceleration')
sgtitle('Raw data, before filtration')

disp("##### Before filtration #####")
disp(['X axis acceleration measurement specification: E = ' num2str(Ex) ', V = ' num2str(Vx) ])
disp(['Y axis acceleration measurement specification: E = ' num2str(Ey) ', V = ' num2str(Vy) ])
disp(['Z axis acceleration measurement specification: E = ' num2str(Ez) ', V = ' num2str(Vz) ])

%% Raw velocity 
% vel_x = cumsum(acc_x) * dt;
% vel_y = cumsum(acc_y) * dt;
% vel_z = cumsum(acc_z) * dt;
% 
% figure()
% plot_them_all(time, vel_x, vel_y, vel_z, 'velocity')
% sgtitle('Raw velocity calculation')
% 
%% Raw position
% pos_x = cumsum(vel_x) * dt;
% pos_y = cumsum(vel_y) * dt;
% pos_z = cumsum(vel_z) * dt;
% 
% % pos_x = trapz(time,vel_x)
% % pos_y = trapz(time,vel_y)
% % pos_z = trapz(time,vel_z)
% 
% figure()
% plot_them_all(time, pos_x, pos_y, pos_z, 'position')
% sgtitle('Raw position calculation')
% 
%% Eliminate bias
% bias_x = mean(acc_x(length(acc_x)-400:length(acc_x)));
% bias_y = mean(acc_y(length(acc_y)-400:length(acc_y)));
% bias_z = mean(acc_z(length(acc_z)-400:length(acc_z)));
bias_x = mean(acc_x(1):acc_x(100));
bias_y = mean(acc_y(length(acc_y)-400:length(acc_y)));
bias_z = mean(acc_z(length(acc_z)-400:length(acc_z)));
disp(['Removing calculated bias error for'...
    '  x: ' num2str(bias_x) ...
    ', y: ' num2str(bias_y) ...
    ', z: ' num2str(bias_z) ])

acc_x = acc_x - bias_x;
acc_y = acc_y - bias_y;
acc_z = acc_z - bias_z;

Ex = mean(acc_x);
Vx = var(acc_x);
Ey = mean(acc_y);
Vy = var(acc_y);
Ez = mean(acc_z);
Vz = var(acc_z);
disp("##### After bias removal #####")
disp(['X axis acceleration measurement specification: E = ' num2str(Ex) ', V = ' num2str(Vx) ])
disp(['Y axis acceleration measurement specification: E = ' num2str(Ey) ', V = ' num2str(Vy) ])
disp(['Z axis acceleration measurement specification: E = ' num2str(Ez) ', V = ' num2str(Vz) ])

%% Filtered acceleration
% figure()
% plot_them_all(time, acc_x, acc_y, acc_z, 'acceleration')
% sgtitle('Filtered data, after removing bias')

%% Remove spikes
acc_x = medfilt1(acc_x, 5);
acc_y = medfilt1(acc_y, 5);
acc_z = medfilt1(acc_z, 5);

Ex = mean(acc_x);
Vx = var(acc_x);
Ey = mean(acc_y);
Vy = var(acc_y);
Ez = mean(acc_z);
Vz = var(acc_z);

disp("##### After filtration #####")
disp(['X axis acceleration measurement specification: E = ' num2str(Ex) ', V = ' num2str(Vx) ])
disp(['Y axis acceleration measurement specification: E = ' num2str(Ey) ', V = ' num2str(Vy) ])
disp(['Z axis acceleration measurement specification: E = ' num2str(Ez) ', V = ' num2str(Vz) ])

%% Filtered acceleration
figure()
plot_them_all(time, acc_x, acc_y, acc_z, 'acceleration')
sgtitle('Filtered data, after removing bias and spikes')

%% Raw velocity 
vel_x = cumsum(acc_x) * dt;
vel_y = cumsum(acc_y) * dt;
vel_z = cumsum(acc_z) * dt;

figure()
plot_them_all(time, vel_x, vel_y, vel_z, 'velocity')
sgtitle('Filtered velocity calculation')

%% Filtered position
pos_x = cumsum(vel_x) * dt;
pos_y = cumsum(vel_y) * dt;
pos_z = cumsum(vel_z) * dt;

figure()
plot_them_all(time, pos_x, pos_y, pos_z, 'position')
sgtitle('Filtered position calculation')

%%
z_to_fft = acc_z;
z_to_del = acc_z < 0.1 & acc_z > -0.1;
z_to_fft(z_to_del) = [];
figure()
plot(z_to_fft)
[y, f] = calc_fft(z_to_fft, freq, 1);

%%
function [] = plot_them_all(time, x, y, z, type)
% PLOT_THEM_ALL Plot values of x, y and z arrays in subplots
%   time    - plot's x values
%   x, y, z - values for certain subplots
%   type    - define type of the plot (also affects titles and labels)
%             possible values: 'acceleration', 'velocity', 'position'

    if type == "position"
        unit = 'm';
    elseif type == "velocity"
        unit = 'm/s';
    elseif type == "acceleration"
        unit = 'm/s^2';
    end
    
    subplot(3,1,1)
    plot(time, x)
    title(['X axis ' type ' calculation'])
    ylabel([type ' [' unit ']'])
    xlabel('time [s]')
    grid minor
    
    subplot(3,1,2)
    plot(time, y)
    title(['Y axis ' type ' calculation'])
    ylabel([type ' [' unit ']'])
    xlabel('time [s]')
    grid minor
    
    subplot(3,1,3)
    plot(time, z)
    title(['Z axis ' type ' calculation'])
    ylabel([type ' [' unit ']'])
    xlabel('time [s]')
    grid minor
end

function [y, f] = calc_fft(x, freq, plotit)
% [y, f] = CALC_FFT Calculate Fast Fourier Transform of signal x, samples with
%                   frequency freq
%   y       - transformed signal in frequency base
%   f       - frequency bases of transformed signal
%   x       - input signal in time base
%   freq    - sampling frequency
%   plotit  - if set to 1, plots a figure of signal's magnitude in
%             frequency base

    f = linspace(0.01, freq/2, length(x));
    y = fft(x);
    
    if (plotit == 1)
        figure()
        plot(f, abs(y))
        grid minor
        xlabel('Frequency [Hz]')
        ylabel('Magnitude')
        xlim([-1 freq/2+1])
        ylim([0 1000])
    end
end