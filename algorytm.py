def _auto_move(self, move_time):
    try:
        # set initial speed and frequency (delay time)
        if self.speed != 0:
            default_speed = round(abs(self.speed))
        else:
            default_speed = 900
        default_freq = round(1/move_time)
        log_info(f'Starting automatic move sequence for motor {self.name} on port: {self.portname} and frequency {self.freq} Hz, speed {default_speed}')
        
        # move to starting position
        with self.semaphore:
            self.motor.multi_position_control(move_time * default_speed + self.zero_position, 200)
        time.sleep(move_time/4)
        
        # start automatic moving sequence
        while self.auto_mode:
            # check if speed or frequency has changed
            if self.speed != default_speed:
                default_speed = round(abs(self.speed))
                log_info(f'Auto mode {self.name} - Auto move speed changed to {default_speed}')
            if self.freq != default_freq:
                move_time = self.freq_to_sec(self.freq)
                default_freq = self.freq
                log_info(f'Auto mode {self.name} - Auto move time changed to {move_time} (freq changed to {self.freq} Hz)')
            
            # move left
            with self.semaphore:
                self.motor.multi_position_control(-1 * move_time * 3/4 * default_speed + self.zero_position, default_speed)
            log_debug(f'Moving motor {self.name} to {-1 * move_time * 3/4 * default_speed + self.zero_position} deg')
            time.sleep(move_time*3/4)
            with self.semaphore:
                self.motor.multi_position_control(-1 * move_time * 7/8 * default_speed + self.zero_position, default_speed*1/2)
            log_debug(f'Moving motor {self.name} to {-1 * move_time * 7/8 * default_speed + self.zero_position} deg')
            time.sleep(move_time/8)
            with self.semaphore:
                self.motor.multi_position_control(-1 * move_time * default_speed + self.zero_position, default_speed*1/4)
            log_debug(f'Moving motor {self.name} to {-1 * move_time * default_speed + self.zero_position} deg')
            time.sleep(move_time/8)

            # move right
            with self.semaphore:
                self.motor.multi_position_control(move_time * 3/4 * default_speed + self.zero_position, default_speed)
            log_debug(f'Moving motor {self.name} to {move_time * 3 / 4 * default_speed + self.zero_position} deg')
            time.sleep(move_time*3/4)
            with self.semaphore:
                self.motor.multi_position_control(move_time * 7/8 * default_speed + self.zero_position, default_speed*1/2)
            log_debug(f'Moving motor {self.name} to {move_time * 7/8 * default_speed + self.zero_position} deg')
            time.sleep(move_time/8)
            with self.semaphore:
                self.motor.multi_position_control(move_time * default_speed + self.zero_position, default_speed*1/4)
            log_debug(f'Moving motor {self.name} to {move_time * default_speed + self.zero_position} deg')
            time.sleep(move_time/8)
    
    except Exception as e:
        log_error(f'Auto mode {self.name} - Automatic move sequence stopped because {e}')
        self.auto_mode = False
    
    # go back to zero position
    with self.semaphore:
        self.motor.multi_position_control(self.zero_position, 200)
    log_warning(f'Auto mode {self.name} -  Leaving automatic move sequence thread of motor {self.name} on port: {self.portname}')