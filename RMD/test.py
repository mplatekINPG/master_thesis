# from multiprocessing import Process, Value
import time
import queue
import threading

## THREADING ##
cmd_queue = queue.PriorityQueue(5000)
ready = True

def generate_value(target_q, priority=0, i=0, wait=5, name="abc"):
    i0 = i
    while ready:
        target_q.put(item=(priority, (name, i0, i)))
        i += 1
        time.sleep(wait)
    print(f'Stopping thread {name}')

def read_value(target_q):
    while ready:
        data = target_q.get()
        print(f'Received item with id: {data[0]}, data: {data[1]}')
        print(f'Queue size is {target_q.qsize()}')
        time.sleep(1)
    print(f'Received stop... Clearing queue')
    start_time = time.time()
    while target_q.qsize() > 0:
        data = target_q.get_nowait()
        print(f'Received item with id: {data[0]}, data: {data[1]}')
        print(f'Queue size is {target_q.qsize()}')
    print(f'It took {time.time() - start_time} seconds to clear queue')
    print(f'Read value thread stopped')

if __name__ == '__main__':
    p1 = threading.Thread(target=generate_value, args=(cmd_queue, 3, 10, 0.1, 'p1', ))
    p2 = threading.Thread(target=generate_value, args=(cmd_queue, 1, 5, 0.5, 'p2', ))
    p3 = threading.Thread(target=generate_value, args=(cmd_queue, 0, 1000, 3, 'p3', ))
    p4 = threading.Thread(target=generate_value, args=(cmd_queue, 3, 100, 1, 'p4', ))

    p_q = threading.Thread(target=read_value, args=(cmd_queue, ))
    
    ready = True
    p_q.start()
    p1.start()
    p2.start()
    p3.start()
    p4.start()

    time.sleep(20)

    ready = False
    p1.join()
    p2.join()
    p3.join()
    p4.join()
    p_q.join()


## MULTIPROCESSING ##

# def f1(f,s,r):
#     i = 0
#     while r.value:
#         print(f'i is {i}, s is {s.value}, waiting {1/f.value}s')
#         time.sleep(1/f.value)
#         i += 1
#     print('F1 done')

# def f2(f,s,r):
#     i = 100
#     while r.value:
#         print(f'i is {i}, s is {s.value}, waiting {1/f.value}s')
#         time.sleep(1/f.value)
#         i += 1
#     print('F2 done')

# if __name__ == '__main__':
#     f = Value('d', 10)
#     s = Value('d', 900)
#     r = Value('d', True)

#     p1 = Process(target=f1, args=(f,s,r,))
#     p2 = Process(target=f2, args=(f,s,r,))

#     p1.start()
#     p2.start()

#     time.sleep(5)
#     f.value = 1

#     time.sleep(5)
#     s.value = 100

#     time.sleep(5)
#     r.value = False

#     print('Main done')