import serial
import time

READ_PID_PARAM                  = 0x30
WRITE_PID_PARAM_TO_RAM          = 0x31
WRITE_PID_PARAM_TO_ROM          = 0x32
READ_ACCELERATION               = 0x33
WRITE_ACCELERATION_TO_RAM       = 0x34
READ_ENCODER                    = 0x90
WRITE_ENCODER_AS_ZERO           = 0x91
WRITE_POSITION_TO_ROM_AS_ZERO   = 0x19
READ_MULTILOOP_ANGLE            = 0x92
READ_SINGLELOOP_ANGLE           = 0x94
READ_MOTOR_STATUS1_AND_ERROR    = 0x9A
CLEAR_MOTOR_ERROR               = 0x9B
READ_MOTOR_STATUS2              = 0x9C
READ_MOTOR_STATUS3              = 0x9D
MOTOR_SHUTDOWN                  = 0x80
MOTOR_STOP                      = 0x81
MOTOR_OPERATION                 = 0x88
OPEN_LOOP_CONTROL               = 0xA0
TORQUE_CONTROL                  = 0xA1
SPEED_CONTROL                   = 0xA2
MULTIPOSITION_CONTROL1          = 0xA3
MULTIPOSITION_CONTROL2          = 0xA4
SINGLEPOSITION_CONTROL1         = 0xA5
SINGLEPOSITION_CONTROL2         = 0xA6
INCREMENTAL_CONTROL1            = 0xA7
INCREMENTAL_CONTROL2            = 0xA8
READ_DRIVER_AND_MOTOR           = 0x12

ROTATION_CW                     = 0X00
ROTATION_CCW                    = 0X01

class RMD():
    _ID = 0x01
    _HEAD_BYTE = 0x3E
    RESPONSE_WAIT_TIME_SEC = 0.0005*2

    ROTATION_CW  = ROTATION_CW
    ROTATION_CCW = ROTATION_CCW

    def __init__(self, id=None, port=None, model='X'):
        """
        Args:
            id (int) : individual identificator for RMD motor, id >= 1
            port (str) : name of port (like 'COMX' or 'dev/ttyX')
        """
        if id:
            self.ID = id
        else:
            self.ID = RMD._ID
            RMD._ID += 0x01

        # port setup
        self.port = serial.Serial(port=port)
        self.port.baudrate = 115200
        self.port.bytesize = serial.EIGHTBITS
        self.port.parity = serial.PARITY_NONE
        self.port.stopbits = serial.STOPBITS_ONE
        self.port.close()

        # motor setup
        self._ready = False
        self._angle = 0.00  # deg
        self._mangle = 0.00  # deg
        self._speed = 0.00  # deg/s
        self._torque = 0.00  # A
        self._encoder = 0
        self._encoder_raw = 0
        self._encoder_offset = 0

        # current current
        self._iA = 0.00  # A
        self._iB = 0.00  # A
        self._iC = 0.00  # A
        
        # motor parameters
        self._voltage = 0  # V
        self._temperature = 0  # deg Celsius
        self.voltage_error = False
        self.temperature_error = False

        # motor controller
        self._reg_position = [100, 100]
        self._reg_speed = [100, 50]
        self._reg_torque = [50, 50]

        # # motor model
        # self._driver_name = None
        # self._motor_name = None
        # self._hardware_version = None
        # self._firmware_version = None

    def start(self):
        """ Start the motor and set ready flag
        """
        try:
            self._send_data(MOTOR_OPERATION)
            self._ready = True
        except:
            self._ready = False
        finally:
            return self._ready

    def stop(self):
        """ Stop the motor (stop without clearing memory) and reset ready flag
        """
        try:
            if self._send_data(MOTOR_STOP):
                self._ready = False       
                return True
        except:
            return False
    
    def shutdown(self):
        """ Shutdown the motor (stop and clear memory) and reset ready flag
        """
        try:
            if self._send_data(MOTOR_SHUTDOWN):
                self._ready = False       
                return True
        except:
            return False

    def clear_errors(self):
        """ Clear motor's error flags
        """
        try:
            if self._send_data(CLEAR_MOTOR_ERROR, True):
                self.voltage_error = False
                self.temperature_error = False
                return True
            return False
        except:
            return False
            
    @property
    def ready(self):
        """ bool: motor is ready for receiving commands.
        
        Setting value to:
            - 1 (or any other positive): run start(),
            - 0 : run stop(),
            - -1 (or any other negative): run shutdown()
        """
        return self._ready

    @ready.setter
    def ready(self, state):
        try:
            if state > 0:
                self.start()
            elif state == 0:
                self.stop()
            else:
                self.shutdown()
        except:
            pass

    @property
    def angle(self):
        """ float: angle of motor's shaft in range 0-359.99 degrees.
        
        Setting angle commands motor to turn to desired angle and hold the position.
        When setting, one can use one or two values:
            - single value is desired angle. If no second argument, motor will turn in shortest way direction,
            - second value is direction: ROTATION_CW or ROTATION_CCW
        Example use:
            motor.angle = 300
            motor.angle = (300, ROTATION_CW)
        """
        try:
            retval = self._get_data(READ_SINGLELOOP_ANGLE)
            if retval:
                self._angle = self.hex2int(retval[::-1])*0.01
        except:
            pass
        finally:
            return self._angle
        
    @angle.setter
    def angle(self, angle_and_direction):
        if isinstance(angle_and_direction, int) or isinstance(angle_and_direction, float):
            desired_angle = angle_and_direction
        else:
            desired_angle = angle_and_direction[0]
        while desired_angle > 359.99:
            desired_angle = desired_angle - 360
        try:
            direction = angle_and_direction[1]
        except:
            direction = RMD.ROTATION_CCW if (desired_angle - self._angle < 180) else RMD.ROTATION_CW
        angle_bytes = bytearray.fromhex(self.int2hex(abs(desired_angle*100)))
        angle_bytes = self.adjust_len(angle_bytes, 2)
        data = [direction, angle_bytes[1], angle_bytes[0], 0x00]
        try:
            self._send_data(SINGLEPOSITION_CONTROL1, data)
            self._angle = desired_angle
        except:
            pass

    @property
    def mangle(self):
        """ float: angle of motor's shaft in wider range of degrees, including negative.
        
        Setting mangle commands motor to turn to desired angle and hold the position.
        """
        try:
            retval = self._get_data(READ_MULTILOOP_ANGLE)
            if retval:
                self._mangle = self.hex2int(retval[::-1], signed=True)*0.01
        except:
            pass
        finally:
            return self._mangle
    
    @mangle.setter
    def mangle(self, desired_angle):
        if desired_angle < 0:
            angle_bytes = bytearray.fromhex(self.int2hex(desired_angle*100, signed=True, nbits=8*8))
        else:
            angle_bytes = bytearray.fromhex(self.int2hex(desired_angle*100))
        angle_bytes = self.adjust_len(angle_bytes, 8)
        data = [angle_bytes[7], angle_bytes[6], angle_bytes[5], angle_bytes[3], 
                angle_bytes[3], angle_bytes[2], angle_bytes[1], angle_bytes[0]]
        try:
            self._send_data(MULTIPOSITION_CONTROL1, data)
            self._mangle = desired_angle
        except:
            pass

    @property
    def speed(self):
        """ float: speed of motor's rotor in degrees per second. 
        Can have positive (counter-clockwise turn) or negative value (clockwise turn).
        
        Setting speed commands motor to turn and keep desired speed.
        """
        try:
            data = self._get_data(READ_MOTOR_STATUS2)
            if data:
                self._temperature = self.hex2int(data[0])
                self._torque = self.hex2int([data[2], data[1]], signed=True) * 33/2048
                self._speed = self.hex2int([data[4], data[3]], signed=True)
                self._encoder = self.hex2int([data[6], data[5]])
        except:
            pass
        finally:
            return self._speed

    @speed.setter
    def speed(self, desired_speed):
        if desired_speed < 0:
            speed_bytes = bytearray.fromhex(self.int2hex(desired_speed*100, signed=True, nbits=4*8))
        else:
            speed_bytes = bytearray.fromhex(self.int2hex(desired_speed*100))
        speed_bytes = self.adjust_len(speed_bytes, 4)
        data = [speed_bytes[3], speed_bytes[2], speed_bytes[1], speed_bytes[0]]
        try:
            self._send_data(SPEED_CONTROL, data)
            self._speed = desired_speed
        except:
            pass

    @property
    def torque(self):
        """ float: motor's torque current in A. 
        Can have positive or negative value.
        
        Setting torque commands motor to keep desired torque current.
        """
        try:
            data = self._get_data(READ_MOTOR_STATUS2)
            if data:
                self._temperature = self.hex2int(data[0])
                self._torque = self.hex2int([data[2], data[1]], signed=True) * 33/2048
                self._speed = self.hex2int([data[4], data[3]], signed=True)
                self._encoder = self.hex2int([data[6], data[5]])
        except:
            pass
        finally:
            return self._torque

    @torque.setter
    def torque(self, desired_current):
        if desired_current < 0:
            current_bytes = bytearray.fromhex(self.int2hex(desired_current/33*2048, signed=True, nbits=2*8))
        else:
            current_bytes = bytearray.fromhex(self.int2hex(desired_current/33*2048))
        current_bytes = self.adjust_len(current_bytes, 2)
        data = [current_bytes[1], current_bytes[0]]
        try:
            self._send_data(TORQUE_CONTROL, data)
            self._torque = desired_current
        except:
            pass

    @property
    def voltage(self):
        """ int: motor's supply voltage
        """
        try:
            data = self._get_data(READ_MOTOR_STATUS1_AND_ERROR)
            if data:
                self._temperature = self.hex2int(data[0])
                self._voltage = round(self.hex2int([data[3], data[2]])*0.1, 2)
                error = self.hex2int(data[6])
                if error == 1:
                    self.voltage_error = True
                elif error == 8:
                    self.temperature_error = True
                elif error == 9:
                    self.voltage_error = True
                    self.temperature_error = True
                else:
                    self.voltage_error = False
                    self.temperature_error = False
        except:
            pass
        finally:
            return self._voltage

    @property
    def temperature(self):
        """ int: motor's actual temperature
        """
        try:
            data = self._get_data(READ_MOTOR_STATUS1_AND_ERROR)
            if data:
                self._temperature = self.hex2int(data[0])
                self._voltage = round(self.hex2int([data[3], data[2]])*0.1, 2)
                error = self.hex2int(data[6])
                if error == 1:
                    self.voltage_error = True
                elif error == 8:
                    self.temperature_error = True
                elif error == 9:
                    self.voltage_error = True
                    self.temperature_error = True
                else:
                    self.voltage_error = False
                    self.temperature_error = False
        except:
            pass
        finally:
            return self._temperature

    @property
    def encoder(self):
        """ int: motor's encoder value.
        Range 0-65k. Value includes encoder offset
        """
        try:
            data = self._get_data(READ_ENCODER)
            if data:
                self._encoder = self.hex2int([data[0], data[1]])
                self._encoder_raw = self.hex2int([data[2], data[3]])
                self._encoder_offset = self.hex2int([data[4], data[5]])
        except:
            pass
        finally:
            return self._encoder
        
    @property
    def encoder_offset(self):
        """ int: offset of motor's encoder value
        """
        try:
            data = self._get_data(READ_ENCODER)
            if data:
                self._encoder = self.hex2int([data[0], data[1]])
                self._encoder_raw = self.hex2int([data[2], data[3]])
                self._encoder_offset = self.hex2int([data[4], data[5]])
        except:
            pass
        finally:
            return self.encoder_offset

    @encoder_offset.setter
    def encoder_offset(self, offset):
        try:
            offset_bytes = bytearray.fromhex(self.int2hex(offset))
            offset_bytes = self.adjust_len(offset_bytes, 2)
            data = [offset_bytes[1], offset_bytes[0]]
            self._send_data(WRITE_ENCODER_AS_ZERO, data)
            self._encoder_offset = offset
        except:
            pass

    @property
    def position_controller(self):
        """ list of ints: parameters of motor's internal position PI controller.
        
        When setting, use two values:
            - proportional gain,
            - integral gain
        """
        try:
            data = self._get_data(READ_PID_PARAM)
            if data:
                self._reg_position[0] = self.hex2int(data[0])
                self._reg_position[1] = self.hex2int(data[1])
                self._reg_speed[0] = self.hex2int(data[2])
                self._reg_speed[1] = self.hex2int(data[3])
                self._reg_torque[0] = self.hex2int(data[4])
                self._reg_torque[1] = self.hex2int(data[5])
        except:
            pass
        finally:
            return self._reg_position

    @position_controller.setter
    def position_controller(self, Kp_and_Ki):
        data = [Kp_and_Ki[0], Kp_and_Ki[1], 
                self._reg_speed[0], self._reg_speed[1], 
                self._reg_torque[0], self._reg_torque[1]]
        try:
            self._send_data(WRITE_PID_PARAM_TO_RAM, data)
            self._reg_position = Kp_and_Ki
        except:
            pass

    @property
    def speed_controller(self):
        """ list of ints: parameters of motor's internal speed PI controller.
        
        When setting, use two values:
            - proportional gain,
            - integral gain
        """
        try:
            data = self._get_data(READ_PID_PARAM)
            if data:
                self._reg_position[0] = self.hex2int(data[0])
                self._reg_position[1] = self.hex2int(data[1])
                self._reg_speed[0] = self.hex2int(data[2])
                self._reg_speed[1] = self.hex2int(data[3])
                self._reg_torque[0] = self.hex2int(data[4])
                self._reg_torque[1] = self.hex2int(data[5])
        except:
            pass
        finally:
            return self._reg_speed

    @speed_controller.setter
    def speed_controller(self, Kp_and_Ki):
        data = [self._reg_position[0], self._reg_position[1],
                Kp_and_Ki[0], Kp_and_Ki[1],  
                self._reg_torque[0], self._reg_torque[1]]
        try:
            self._send_data(WRITE_PID_PARAM_TO_RAM, data)
            self._reg_speed = Kp_and_Ki
        except:
            pass

    @property
    def torque_controller(self):
        """ list of ints: parameters of motor's internal torque PI controller.
        
        When setting, use two values:
            - proportional gain,
            - integral gain
        """
        try:
            data = self._get_data(READ_PID_PARAM)
            if data:
                self._reg_position[0] = self.hex2int(data[0])
                self._reg_position[1] = self.hex2int(data[1])
                self._reg_speed[0] = self.hex2int(data[2])
                self._reg_speed[1] = self.hex2int(data[3])
                self._reg_torque[0] = self.hex2int(data[4])
                self._reg_torque[1] = self.hex2int(data[5])
        except:
            pass
        finally:
            return self._reg_torque

    @torque_controller.setter
    def torque_controller(self, Kp_and_Ki):
        data = [self._reg_position[0], self._reg_position[1],
                self._reg_speed[0], self._reg_speed[1], 
                Kp_and_Ki[0], Kp_and_Ki[1]]
        try:
            self._send_data(WRITE_PID_PARAM_TO_RAM, data)
            self._reg_torque = Kp_and_Ki
        except:
            pass

    @property
    def current(self):
        """ list of floats: current on motor's phases A, B anc C
        """
        try:
            data = self._get_data(READ_MOTOR_STATUS3)
            if data:
                self._temperature = self.hex2int(data[0])
                self._iA = self.hex2int([data[2], data[1]]) * 1/64
                self._iB = self.hex2int(data[4], data[3]) * 1/64
                self._iC = self.hex2int(data[6], data[5]) * 1/64
        except:
            pass
        finally:
            return self._iA, self._iB, self._iC
        
    def _send_data(self, cmd, data=None, clear_response=True):
        """ Send command with data to RMD driver

        Args:
            cmd (hex) : hex of command
            data (list) : list of hexes as command data
        Returns:
            (bool|list) : False if fail, True if sent and clear_response=True, response otherwise
        """
        frame = self._create_frame_data(cmd, data)
        if not self.port.is_open:
            self.port.open()
        if self._send_command(frame):
            ret = self._handle_command_response(clear=clear_response)
            if self.port.is_open:
                self.port.close()
            return ret
        else:
            if self.port.is_open:
                self.port.close()
            return False

    def _get_data(self, cmd):
        """ Receive data from RMD driver based on cmd

        Args:
            cmd (hex) : hex of command
        Returns:
            (bool|list) : False if failed, data list otherwise
        """
        frame = self._create_frame_data(cmd)
        if not self.port.is_open:
            self.port.open()
        if self._send_command(frame):
            ret = self._handle_command_response()
            if self.port.is_open:
                self.port.close()
            return ret
        else:
            if self.port.is_open:
                self.port.close()
            return False

    def _handle_command_response(self, clear=False):
        """ Wait for incoming message and handle it properly

        Args:
            clear (bool) : if input buffer should be cleared instead of reading
        Returns:
            (bool|list) : True|False if no data to return, data list otherwise
        """
        ret = []
        if clear:
            self.port.reset_input_buffer()
        while not self.port.in_waiting > 0:
            time.sleep(RMD.RESPONSE_WAIT_TIME_SEC)
        if clear:
            self.port.reset_input_buffer()
            return True
        else:
            ret = self._read_frame()
            if ret:
                return ret
            return False

    def _send_command(self, frame):
        """ Send command frame to RMD driver

        Args:
            frame (bytearray) : RMD frame to send
        Returns:
            (bool) : wheter number of sent bytes equals frame length
        """
        ret = self.port.write(frame)
        if ret == len(frame):
            return True
        else:
            return False

    def _read_frame(self):
        """ Try to read from waiting serial buffer

        Returns:
            One of following:
                (int|list) : 0 if data not received, -1 if not ok, data list otherwise 
        """
        if self.port.in_waiting:
            header = self.port.read(5)
            header_fields = list(header)
            if header_fields[3] > 0x00:
                data = self.port.read(header_fields[3]+1)
            if data:
                received = header + data
            else:
                received = header
            frame_ok, parsed = self._parse_frame(received)

            if frame_ok:
                return parsed
            else:
                return -1
        else:
            return False

    def _create_frame_data(self, cmd, data=None):
        """ Create RMD frame based on passed command hex

        Args:
            cmd (hex) : hex of command
            data (list) : list of hexes as command data
        Returns:
            (bytearray) : RMD frame to send
        """
        frame = bytearray()
        size = 1+1+1+1+1 # head + command + ID + length + check
        data_length = 0
        if data:
            data_length = len(data)
            size += data_length # data size
            size += 1 # check

        head_checksum = (RMD._HEAD_BYTE + cmd + self.ID + data_length) % 256
        if data:
            data_checksum = sum(data) % 256

        frame.append(RMD._HEAD_BYTE)
        frame.append(cmd)
        frame.append(self.ID)
        frame.append(data_length)
        frame.append(head_checksum)
        if data:
            frame.extend(data)
            frame.append(data_checksum)
        return frame

    def _parse_frame(self, frame):
        """ Parse received frame from RS485

        Args:
            frame (bytearray) : received data
        Returns:
            (bool) : wheter received frame was ok (id, checksums)
            (list|None) : received data if any, None otherwise
        """
        fields = list(frame)
        id = fields[2]
        data_length = fields[3]
        head_checksum = fields[4]
        data_received = list()
        if data_length > 0:
            for i in range(data_length):
                data_received.append(frame[5+i])
            data_checksum = fields[4+data_length+1]
        
        if id != self.ID:
            print('Invalid id')
            return False, []

        if head_checksum != sum(fields[0:4]) % 256:
            print(f'Invalid head checksum: {head_checksum} instead of {sum(fields[0:3])}')
            return False, []

        if data_checksum and data_checksum != sum(data_received) % 256:
            print(f'Invalid data checksum: {data_checksum} instead of {sum(data_received)%256} with data {data_received}')
            return False, []

        if data_received:
            return True, data_received
        else:
            return True, []

    @staticmethod
    def int2hex(intnum, signed=False, nbits=0):
        """ Convert integer to hex understable by bytearray

        Args:
            intnum (int) : integer to convert
            signed (bool) : if number is negative
            nbits (int) : for negative number, number of bits to write value to (bytes*4)
        Returns:
            (str) : string of hex value without '0x' prefix
        """
        if not signed:
            hexstr = hex(round(intnum)).replace('0x','')
        else:
            hexstr = hex((round(intnum) + (1 << nbits)) % (1 << nbits))
            hexstr = hexstr.replace('0x','')

        if (len(hexstr) % 2 == 1):
            hexstr = '0' + hexstr
        return hexstr

    @staticmethod
    def hex2int(hexnum, signed=False):
        """ Convert hex string or list of hex strings into integer
        
        Args:
            hexnum (str|list) : hex string or list
            signed (bool) : for converting from negative number
        Returns:
            (int) : integer number
        """
        if not isinstance(hexnum, list):
            hexnum = [hexnum]
        intnum = 0
        bits = len(hexnum)*8
        for i in range(len(hexnum)):
            intnum += hexnum[-i-1] << (8*i)
        if signed and (intnum & (1 << (bits-1))):
            intnum -= 1 << bits
        return intnum

    @staticmethod
    def adjust_len(array, desire_len):
        """ Adjust length of input array to desired length in bytes
        
        Args:
            array (hex int) : input number
            desire_len (int) : desired length of output in bytes
        Returns:
            (hex) : hex adjusted by adding leading zeros
        """
        actual_len = len(array)
        while actual_len < desire_len:
            array.insert(0,0)
            actual_len = len(array)
        return array

if __name__ == '__main__':
    pass
