#include <Arduino.h>
#include <PubSubClient.h>
#include <WiFi.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>

//#define INTERRUPT_MODE
#ifdef INTERRUPT_MODE
int data_ready_pin = 23;
#endif

// tasks 
TaskHandle_t Task1;
TaskHandle_t Task2;
void BNOMeasurement(void * parameter);
void MQTTPublish(void * parameter);
#define BUFFER_SIZE 9
#define ELEMENTS_IN_PART 4
QueueHandle_t queue0;
QueueHandle_t queue1;
SemaphoreHandle_t sem_q0;
SemaphoreHandle_t sem_q1;
void SEMsetup();
void QUEUEsetup();

// MQTT connection
char ssid[] = "Nadajnik 5G KRK13 500% mocy";
char pass[] = "3Kotkisierotki";
int status = WL_IDLE_STATUS;
WiFiClient espClient;
PubSubClient client(espClient);
//char mqtt_server[] = "broker.mqttdashboard.com";
IPAddress mqtt_server(192, 168, 0, 130);
char mqtt_topic[] = "mgr/meas/bno";
void mqtt_reconnect();
void WiFisetup();
void MQTTsetup();

// BNO
#define IMU_SELECT 1 // 1 - BNO055, 0 - MPU9250
TwoWire I2CBNO = TwoWire(1);
Adafruit_BNO055 bno = Adafruit_BNO055(55, 0x28, &I2CBNO);
#define I2C_SDA 21
#define I2C_SCL 22
sensors_event_t AccData;
void BNOsetup();


void setup() {
  Serial.begin(115200);
  
  #ifdef INTERRUPT_MODE
  pinMode(data_ready_pin, INPUT);
  #endif

  Serial.println("BNO initialization....");
  while (!I2CBNO.begin(I2C_SDA, I2C_SCL, 100000)) {
  //while (!Wire.begin()) {
    Serial.println("I2C failed, trying again");
    delay(2000);
  } 
  Serial.println("I2C set...");
  delay(500);
  BNOsetup();
  Serial.println("BNO begin success");
  
  Serial.println("WiFi and MQTT initialization....");
  WiFisetup();
  MQTTsetup();
  Serial.println("WiFi and MQTT initialization success");
  delay(500);

  Serial.println("Semaphores initialization....");
  SEMsetup();
  Serial.println("Semaphores initialization success");
  delay(500);

  Serial.println("Queues initialization....");
  QUEUEsetup();
  Serial.println("Queues initialization success");
  delay(500);

  Serial.println("Tasks initialization....");
  xTaskCreatePinnedToCore(BNOMeasurement, "Task1", 4096, NULL, 1, &Task1, 0);
  delay(500);
  xTaskCreatePinnedToCore(MQTTPublish, "Task2", 4096, NULL, 1, &Task2, 1);
  delay(500);
  Serial.println("Tasks initialization success");
  delay(500);
}

void loop() {
  // task 1 on core 0 : measurment to one of two buffers
  // task 2 on core 1 : mqtt sending
}

void BNOsetup() {
  // I2CBNO.begin(I2C_SDA, I2C_SCL, 100000);
  while(!bno.begin()) {
    Serial.println("BNO failed to begin");
    delay(2000);
  }
}

void WiFisetup() {
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    status = WiFi.begin(ssid, pass);
    delay(5000);
  }
  Serial.print("Wifi Connected with IP: ");
  IPAddress ip = WiFi.localIP();
  Serial.println(ip);
}

void MQTTsetup() {
  client.setServer(mqtt_server, 1883);
  mqtt_reconnect();
}

void mqtt_reconnect() {
  while (!client.connected())  {
    if(client.connect("espClient")) {
      Serial.println("Connected to MQTT broker");
    }
    else {
      Serial.println("Failed... Trying again in 2 seconds");
      delay(2000);
    }
  }
}

void SEMsetup() {
  sem_q0 = xSemaphoreCreateBinary();
  sem_q1 = xSemaphoreCreateBinary();
  if (sem_q0 == NULL || sem_q1 == NULL) {
    Serial.println("Error creating one of semaphores");
  }
}

void QUEUEsetup() {
  queue0 = xQueueCreate(BUFFER_SIZE*ELEMENTS_IN_PART, sizeof(float));
  queue1 = xQueueCreate(BUFFER_SIZE*ELEMENTS_IN_PART, sizeof(float));
  if (queue0 == NULL || queue1 == NULL) {
    Serial.println("Error creating one of queues");
  }
}

void BNOMeasurement(void * parameter) {
  int lastQ = 1;
  float time = 0;
  for(;;) {
    if (lastQ == 1) {
      for (int i=0; i<BUFFER_SIZE; i++) {
        #ifdef INTERRUPT_MODE
        while(!digitalRead(data_ready_pin) != LOW) 
        {;}
        #endif

        bno.getEvent(&AccData, Adafruit_BNO055::VECTOR_ACCELEROMETER);
        xQueueSend(queue0, &time, portMAX_DELAY);
        xQueueSend(queue0, &AccData.acceleration.x, portMAX_DELAY);
        xQueueSend(queue0, &AccData.acceleration.y, portMAX_DELAY);
        xQueueSend(queue0, &AccData.acceleration.z, portMAX_DELAY);
        time = int(time)%60000 + 1;

        #ifdef INTERRUPT_MODE
        vTaskDelay(5 / portTICK_PERIOD_MS);
        #else
        vTaskDelay(50 / portTICK_PERIOD_MS);
        #endif
        }  
      xSemaphoreGive(sem_q0);
    } 
    else if (lastQ == 0) {
      for (int i=0; i<BUFFER_SIZE; i++) {
        #ifdef INTERRUPT_MODE
        while(!digitalRead(data_ready_pin) != LOW) 
        {;}
        #endif
        bno.getEvent(&AccData, Adafruit_BNO055::VECTOR_ACCELEROMETER);
        xQueueSend(queue1, &time, portMAX_DELAY);
        xQueueSend(queue1, &AccData.acceleration.x, portMAX_DELAY);
        xQueueSend(queue1, &AccData.acceleration.y, portMAX_DELAY);
        xQueueSend(queue1, &AccData.acceleration.z, portMAX_DELAY);
        time = int(time)%60000 + 1;
        
        #ifdef INTERRUPT_MODE
        vTaskDelay(5 / portTICK_PERIOD_MS);
        #else
        vTaskDelay(50 / portTICK_PERIOD_MS);
        #endif
      }  
      xSemaphoreGive(sem_q1);
    }
    (lastQ == 0) ? lastQ = 1 : lastQ = 0; 
  }
}

void MQTTPublish(void * parameter) {
  float element;
  for (;;) {
    //if (xSemaphoreTake(sem_q0, portMAX_DELAY) == pdPASS) {
    if (uxQueueMessagesWaiting(queue0) >= BUFFER_SIZE*ELEMENTS_IN_PART) {
      String msg;
      for (int i=0; i<BUFFER_SIZE*ELEMENTS_IN_PART; i++) {
        xQueueReceive(queue0, &element, portMAX_DELAY);
        msg += element;
        (i%ELEMENTS_IN_PART == ELEMENTS_IN_PART-1) ? msg += ";" : msg += ",";
      }
      char msg_to_send[msg.length()+1];
      msg.toCharArray(msg_to_send, msg.length()+1);
      client.publish(mqtt_topic, msg_to_send);
      Serial.print("Queue0 is: ");
      Serial.println(msg_to_send);
    }
    
    //if (xSemaphoreTake(sem_q1, portMAX_DELAY) == pdPASS) {
    if (uxQueueMessagesWaiting(queue1) >= BUFFER_SIZE*ELEMENTS_IN_PART) {
      String msg;
      for (int i=0; i<BUFFER_SIZE*ELEMENTS_IN_PART; i++) {
        xQueueReceive(queue1, &element, portMAX_DELAY);
        msg += element;
        (i%ELEMENTS_IN_PART == ELEMENTS_IN_PART-1) ? msg += ";" : msg += ",";
      }
      char msg_to_send[msg.length()+1];
      msg.toCharArray(msg_to_send, msg.length()+1);
      client.publish(mqtt_topic, msg_to_send);
      Serial.print("Queue1 is: ");
      Serial.println(msg_to_send);
    }
  }
}