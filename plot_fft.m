function fig = plot_fft(x, fs)
    T = 1/fs;
    L = length(x);
    
    y = fft(x);
    P2 = abs(y/L);
    P1 = P2(1:L/2+1); 
    P1(2:end-1) = 2*P1(2:end-1);
    f = fs*(0:(L/2))/L;
    plot(f, P1)
    ylim([0, max(P1(P1<max(P1)))*1.1])
end