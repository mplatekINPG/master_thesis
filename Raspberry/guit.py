# from guietta import Gui, _

# def recalc(gui, *args):
#     gui.result = float(gui.num)*2

# gui = Gui( 
#     [ 'Enter number', '__num__', ['Go'] ],
#     [ 'Result: ', 'result', _ ] )

# gui.events([ _ , _ , recalc ],
#            [ _ , _,  _ ] )

# from guietta import Gui, _, HSeparator, C, R, ___, III, QMessageBox


# main = Gui(['<center>Dynamic layout demonstration</center'],
#            [HSeparator],
#            ['label1', 'label2', ['Click here'] ],
#            ['label3',   ___   ,        _       ],
#            [  III   ,   III   ,    'label4'    ],
#            ['label5', 'label6',        _       ])

# subgui = Gui( [C('checkbox'), '__editbox__'],
#               [R('radio')   ,  ['And here!']  ])

# subgui.Andhere = lambda x: QMessageBox.information(None, "Clicked",
#                            "The editbox contains "+subgui.editbox)

# with main.Clickhere:
#     if main.is_running:
#         main.label3 = subgui


# main.run()

# from guietta import Gui, _, HSeparator, C, G, R, ___, III, QMessageBox


# main = Gui(['<center>Dynamic layout demonstration</center'],
#            [HSeparator],
#            [   'label1'    , 'label2', ['Click here'] ],
#            [G('Inner GUI') ,   ___   ,        _       ],
#            [     III       ,   III   ,    'label4'    ],
#            [   'label5'    , 'label6',        _       ])

# subgui = Gui( [C('checkbox'), '__editbox__'],
#               [R('radio')   ,  ['And here!']  ])

# subgui.Andhere = lambda x: QMessageBox.information(None, "Clicked",
#                            "The editbox contains "+subgui.editbox)

# with main.Clickhere:
#     if main.is_running:
#         main.InnerGUI = subgui


# main.run()

# from guietta import Gui, LB, _, III

# def listbox(gui, text):
#     print(text)

# def test(gui, *args):
#     print(gui.listbox)
    

# gui = Gui(
    
#   [ LB('listbox') ],
#   [       III     ],
#   [       III     ],
#   [     ['Test']  ])


# gui.events(
    
#     [   listbox  ],
#     [      _     ],
#     [      _     ],
#     [     test   ],
#    )

# gui.widgets['listbox'].setSelectionMode(gui.widgets['listbox'].ExtendedSelection)

# gui.listbox = ['a', 'b', 'c', 'd']

# gui.run()


# from guietta import Gui, B, E, L, HS, VS, HSeparator, VSeparator
# from guietta import Yes, No, Ok, Cancel, Quit, _, ___, III
# from guietta import R1, R2, C, P

# try:
#     from PySide2.QtWidgets import QDial, QLCDNumber, QTableWidget
#     from PySide2.QtWidgets import QTableWidgetItem, QHeaderView
# except ImportError:
#     from PyQt5.QtWidgets import QDial, QLCDNumber, QTableWidget
#     from PyQt5.QtWidgets import QTableWidgetItem, QHeaderView


# gui = Gui(

#   [ '<center>A big GUI with all of Guietta''s widgets</center>'],
#   [ '<center>Move the dial!</center>'],
#   [ HSeparator ],

#   [ 'Label'    , 'imagelabel.jpeg' , L('another label')  , VS('slider1')],
#   [  _         ,     ['button']    , B('another button') ,     III      ],
#   [ '__edit__' ,  E('an edit box') , _                   ,   VSeparator ],
#   [ R1('rad1') ,  R1('rad2')       , R1('rad3')          ,     III      ],
#   [ R2('rad4') ,  R2('rad5')       , R2('rad6')          ,     III      ],
#   [ C('ck1')   ,   C('ck2')        , C('ck3')            ,     III      ],
#   [   Quit     ,        Ok         , Cancel              ,     III      ],
#   [    Yes     ,        No         , _                   ,     III      ],
#   [  HS('slider2'),    ___         , ___                 ,      _       ],
#   [  (QDial, 'dial'),  (QLCDNumber, 'lcd')   , ___       ,      _       ],
#   [  (QTableWidget, 'tab1'),  ___      , ___             ,      ___     ],
#   [    III     , III , III , III ],
#   [    III     , III , III , III ],
#   [P('progbar'), (QLCDNumber, 'lcd2')   , _   ,  _    ],
#   [   L('l1'),  L('l2'), L('l3'), L('l4')      ],
# )

# gui.window().setGeometry( 100, 100, 600, 900 )      # posx, posy, w, h

# gui.widgets['dial'].setNotchesVisible( True )

# gui.widgets['tab1'].setColumnCount( 4 )
# gui.widgets['tab1'].setRowCount( 5 )
# gui.widgets['tab1'].horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

# gui.l1 = "Label 1"
# gui.l2 = "This is Label 2"

# for x in range( 4 ):
#     for y in range( 5 ):
#         item = QTableWidgetItem('%d %d' % (x, y))
#         gui.widgets['tab1'].setItem( y, x, item )
#         gui.widgets['tab1'].setHorizontalHeaderItem( x, QTableWidgetItem('Col: %d' % x ) )
#         gui.widgets['tab1'].setVerticalHeaderItem( y, QTableWidgetItem('Row: %d' % y ) )

# while True:
#     name, event = gui.get()

#     if name == 'slider1':
#         gui.lcd.display( float(gui.slider1))
#         gui.widgets['tab1'].itemAt( 0, 0 ).setText(str(gui.slider1))

#     elif name == 'dial':
#         gui.widgets['tab1'].itemAt( 0, 1 ).setText(str(gui.dial))
#         gui.lcd2.display( float(gui.dial))
#         gui.progbar = gui.dial

#     elif name == None:
#         break


from guietta import Gui, _, ___, III, HValueSlider, VValueSlider, Qt

def do_slider(gui, value):
    print('Slider value: ', value)
    
slider1 = HValueSlider('hvalue', range(500), unit='Hz')    
slider2 = HValueSlider('hvalue', range(500), unit='Hz', anchor=Qt.AnchorLeft) 
slider3 = VValueSlider('hvalue', range(500))    

gui = Gui(
    
  [  'xxx' , ['xxx'], 'xxx' , slider3],
  [  slider1,   ___   , ___ ,   III  ],
  [  slider2,   ___   ,  _  ,   III  ],
  )


gui.events(
    
  [     _     ,    _    ,   _  ,  _  ],
  [  do_slider,    _    ,   _  ,  _  ],
  [     _     ,    _    ,   _  ,  _  ],
   )


gui.run()