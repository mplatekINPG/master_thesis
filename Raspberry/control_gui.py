from guietta import Gui, _, G, VS, HS, C, R, HSeparator, ___, III, splash

MainGroups = Gui(

    ['<center>Car simulator controller</center>', ___, ___, ___, ___],
    [HSeparator],
    [_, ['AutoStart'], ['AutoStop'], ['ReadAll'], _],
    [G('Options'), G('Control'), G('Status'), G('PID'), G('Other')],

)

MainButtons = Gui(

    ['<center>Car simulator controller</center>', ___, ___, ___, ___],
    [HSeparator],
    [_, ['AutoStart'], ['AutoStop'], ['ReadAll'], _],
    [['Options'], ['Control'], ['Status'], ['PID'], ['Other']],
    [G('subgroup'), ___, ___, ___, ___]

)

OptionsV = Gui(
    [ ['Turn off'] ], 
    [  ['Restart'] ], 
    [   ['About']  ]
)

OptionsH = Gui(
    [ ['Turn off'], ['Restart'], ['About'] ]
)

OtherV = Gui(
    [C('Sound environment')], 
    [           _          ], 
    [C('Light environment')]
)

OtherH = Gui(
    [C('Sound environment'), _, C('Light environment')]
)

Control = Gui(
    [G('Motor1'), G('Motor2'), G('Motor3')]
)

ControlM1 = Gui (
    [['Start'], _, _], 
    [['Stop'], _, _],
    [['Shutdown'], _, _],
    [['Auto'], '__hz__', 'Hz'],
    [['Speed'], '__speed__', 'deg/s']
)

ControlM2 = Gui (
    [['Start'], _, _], 
    [['Stop'], _, _],
    [['Shutdown'], _, _],
    [['Auto'], '__hz__', 'Hz'],
    [['Speed'], '__speed__', 'deg/s']
)

ControlM3 = Gui (
    [['Start'], _, _], 
    [['Stop'], _, _],
    [['Shutdown'], _, _],
    [['Auto'], '__hz__', 'Hz'],
    [['Speed'], '__speed__', 'deg/s']
)

Status = Gui(
    [G('Motor1'), G('Motor2'), G('Motor3')]
)

StatusM1 = Gui(
    ['Angle:', 'angle', 'deg'],
    ['MAngle:', 'mangle', 'deg'],
    ['Voltage:', 'voltage', 'V'],
    ['Temp.:', 'temp', '*C']
)

StatusM2 = Gui(
    ['Angle:', 'angle', 'deg'],
    ['MAngle:', 'mangle', 'deg'],
    ['Voltage:', 'voltage', 'V'],
    ['Temp.:', 'temp', '*C']
)

StatusM3 = Gui(
    ['Angle:', 'angle', 'deg'],
    ['MAngle:', 'mangle', 'deg'],
    ['Voltage:', 'voltage', 'V'],
    ['Temp.:', 'temp', '*C']
)

PID = Gui(
    [G('Motor1'), G('Motor2'), G('Motor3')]
)

PIDM1 = Gui(
    ['Pos Kp:', '__pkp__'],
    ['Pos Ki:', '__pki__'],
    ['Spd Kp:', '__skp__'],
    ['Spd Ki:', '__ski__'],
    ['Trq Kp:', '__tkp__'],
    ['Trq Ki:', '__tki__']
)

PIDM2 = Gui(
    ['Pos Kp:', '__pkp__'],
    ['Pos Ki:', '__pki__'],
    ['Spd Kp:', '__skp__'],
    ['Spd Ki:', '__ski__'],
    ['Trq Kp:', '__tkp__'],
    ['Trq Ki:', '__tki__']
)

PIDM3 = Gui(
    ['Pos Kp:', '__pkp__'],
    ['Pos Ki:', '__pki__'],
    ['Spd Kp:', '__skp__'],
    ['Spd Ki:', '__ski__'],
    ['Trq Kp:', '__tkp__'],
    ['Trq Ki:', '__tki__']
)

Empty = Gui(
    [_]
)

MainGroups.Options = OptionsV
MainGroups.Other = OtherV
MainGroups.Control = Control
MainGroups.Status = Status
MainGroups.PID = PID

Control.Motor1 = ControlM1
ControlM1.hz = 10
ControlM1.speed = 0
Control.Motor2 = ControlM2
ControlM2.hz = 10
ControlM2.speed = 0
Control.Motor3 = ControlM3
ControlM3.hz = 10
ControlM3.speed = 0

Status.Motor1 = StatusM1
StatusM1.angle = 0
StatusM1.mangle = 0
StatusM1.voltage = 0
StatusM1.temp = 0
Status.Motor2 = StatusM2
StatusM2.angle = 0
StatusM2.mangle = 0
StatusM2.voltage = 0
StatusM2.temp = 0
Status.Motor3 = StatusM3
StatusM3.angle = 0
StatusM3.mangle = 0
StatusM3.voltage = 0
StatusM3.temp = 0

PID.Motor1 = PIDM1
PIDM1.pkp = 100
PIDM1.pki = 100
PIDM1.skp = 100
PIDM1.ski = 100
PIDM1.tkp = 100
PIDM1.tki = 50
PID.Motor2 = PIDM2
PIDM2.pkp = 100
PIDM2.pki = 100
PIDM2.skp = 100
PIDM2.ski = 100
PIDM2.tkp = 100
PIDM2.tki = 50
PID.Motor3 = PIDM3
PIDM3.pkp = 100
PIDM3.pki = 100
PIDM3.skp = 100
PIDM3.ski = 100
PIDM3.tkp = 100
PIDM3.tki = 50

MainGroups.window().setGeometry( 0, 0, 480, 300 )
MainGroups.window().resize(480, 300)

MainGroups.window().layout().itemAt(0).widget().deleteLater()

MainGroups.run()

# with MainButtons.Options:
#     if MainButtons.is_running:
#         #print(MainButtons.subgroup._widget.layout())
#         # if MainButtons.subgroup._widget.layout(): 
#         #     for i in reversed(range(MainButtons.subgroup._widget.layout().count())):
#         # #         MainButtons.subgroup._widget.layout().itemAt(i).widget().setParent(None)
#         # if MainButtons.subgroup._widget.layout(): 
#         #     for i in reversed(range(MainButtons.subgroup._widget.layout().count())):
#         #         MainButtons.subgroup._widget.layout().itemAt(i).widget().setParent(None)
#         #     MainButtons.subgroup._widget.layout().deleteLater()
#         MainButtons.subgroup = OptionsV
#         MainButtons.subgroup = 'Options'

# with MainButtons.Other:
#      if MainButtons.is_running:
#         #print(MainButtons.subgroup._widget.layout())
#         if MainButtons.subgroup._widget.layout(): 
#             MainButtons.subgroup._widget.itemAt(0).widget().setParent(None)
#         #     for i in reversed(range(MainButtons.subgroup._widget.layout().count())):
#         #         MainButtons.subgroup._widget.layout().itemAt(i).widget().setParent(None)
#         #     MainButtons.subgroup._widget.layout().deleteLater()
#         #     # MainButtons.subgroup._widget.layout().itemAt(0).widget().setParent(None)
#         MainButtons.subgroup = OtherV
#         MainButtons.subgroup = 'Other'

# with MainButtons.PID:
#     if MainButtons.is_running:
#         MainButtons.subgroup = PID
#         MainButtons.subgroup = 'PID'

# with MainButtons.Control:
#     if MainButtons.is_running:
#         MainButtons.subgroup = Control
#         MainButtons.subgroup = 'Control'

# with MainButtons.Status:
#     if MainButtons.is_running:
#         MainButtons.subgroup = Status
#         MainButtons.subgroup = 'Status'

# MainButtons.run()