import wx
import os
import sys
import vlc
import time
import queue
import datetime
import threading
from RMD import rmd
from math import floor

################################## LOGGING #####################################
def log_info(msg):
    sys.stdout.write(f'INFO:{msg}')

def log_error(msg):
    sys.stdout.write(f'ERROR:{msg}')

def log_warning(msg):
    sys.stdout.write(f'WARNING:{msg}')

def log_debug(msg):
    sys.stdout.write(f'DEBUG:{msg}')

class MyLogger():
    def __init__(self, level='INFO'):
        timestamp = datetime.date.today().strftime("%Y-%m-%d")
        # logfile = '/home/pi/Desktop/logs/ControlPanel-' + timestamp + '.log'
        logfile = 'ControlPanel-' + timestamp + '.log'
        self.log = open(logfile, 'a')
        self.log.write('\n')
        if level is 'DEBUG':
            self.levels = ('DEBUG', 'INFO', 'WARNING', 'ERROR')
        elif level is 'INFO':
            self.levels = ('INFO', 'WARNING', 'ERROR')
        elif level is 'WARNING':
            self.levels = ('WARNING', 'ERROR')
        elif level is 'ERROR':
            self.levels = ('ERROR')
        else:
            self.levels = ('DEBUG', 'INFO', 'WARNING', 'ERROR')

    def write(self, message):
        timestamp = datetime.datetime.strftime(datetime.datetime.now(), 
                                               ' %Y-%m-%d %H:%M:%S')
        if 'ERROR' in message:
            msg = message.replace('ERROR:', '')
            self.log.write(timestamp + ' - ERROR : ' + msg + '\n')
        elif 'WARNING' in message: 
            msg = message.replace('WARNING:', '')
            if 'WARNING' in self.levels:
                self.log.write(timestamp + ' - WARNING : ' + msg + '\n')
        elif 'INFO' in message:
            msg = message.replace('INFO:', '')
            if 'INFO' in self.levels:
                self.log.write(timestamp + ' - INFO : ' + msg + '\n')
        elif 'DEBUG' in message:
            msg = message.replace('DEBUG:', '')
            if 'DEBUG' in self.levels:
                self.log.write(timestamp + ' - DEBUG : ' + msg + '\n')
        else:
            self.log.write(message)
        self.log.flush()

    def flush(self):
        self.log.flush()
        os.fsync(self.log.fileno())

    def close(self):
        self.log.close()
##############################################################################

class MyFrameWithFont(wx.Frame):
    def __init__(self, title, size):
        wx.Frame.__init__(self, None, title=title, size=size)
        myfont = wx.Font(7, wx.DEFAULT, wx.NORMAL, wx.NORMAL, False)
        self.SetFont(myfont)

class MyPanelWithFont(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        myfont = wx.Font(7, wx.DEFAULT, wx.NORMAL, wx.NORMAL, False)
        self.SetFont(myfont)

commands_queue = queue.PriorityQueue()
queue_processing = False
m1_queue = queue.Queue()
m2_queue = queue.Queue()
m3_queue = queue.Queue()

class Frame(wx.Frame):
    WIDTH = 480
    HEIGHT = 280 #360
    PASSWORD = 'raspberry'

    def __init__(self, title):
        wx.Frame.__init__(self, None, title=title, size=(Frame.WIDTH,Frame.HEIGHT))

        log_debug(f'Main Frame - Creating message queue')
        port = None # '/dev/ttyUSB0
        m_queues = [m1_queue, m2_queue, m3_queue]
        global queue_processing
        queue_processing = True
        self.ready = True
        self.thread = threading.Thread(target=self.queue_handler, args=(commands_queue, port, m_queues, ), daemon=True)
        self.thread.start()

        log_debug(f'Main Frame - Creating list of motors')
        motors = list()
        motors.append(self.RMDControls(0x02, port, 'back', commands_queue, m2_queue))
        motors.append(self.RMDControls(0x01, port, 'left', commands_queue, m1_queue))
        motors.append(self.RMDControls(0x03, port, 'right', commands_queue, m3_queue))

        log_debug(f'Main Frame - Creating notebook in frame\'s panel')
        panel = wx.Panel(self)
        notebook = wx.Notebook(panel)

        log_debug(f'Main Frame - Creating tabs in notebook')
        tabControl = ControlTab(notebook, motors)
        tabPID = PIDTab(notebook, motors)
        tabStat = StatusTab(notebook, motors)
        tabOptions = OptionsTab(notebook)
        tabEnv = EnvTab(notebook)

        notebook.AddPage(tabOptions, 'Options')
        notebook.AddPage(tabControl, 'Control')
        notebook.AddPage(tabStat, 'Status')
        notebook.AddPage(tabPID, 'PID')
        notebook.AddPage(tabEnv, 'Other')

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(notebook, 1, wx.EXPAND)
        panel.SetSizer(sizer)

        self.Bind(wx.EVT_CLOSE, self.OnClose)
        log_debug('Main Frame  - Created')
        try:
            os.system('matchbox-keyboard &')
            log_debug('Opening keyboard')
        except:
            log_debug('Could not open keyboard')

    def OnClose(self, event):
        log_info('\'X\' button clicked, requesting window close')
        dialog_box = wx.MessageDialog(self, "Do you really want to close application?", 'Closing window', wx.YES|wx.NO|wx.ICON_QUESTION)
        result = dialog_box.ShowModal()
        dialog_box.Destroy()
        if result == wx.ID_YES:
            log_info('Main Frame - Closed. Goodbye')
            self.ready = False
            self.thread.join(1)
            try:
                os.system('pkill -f matchbox-keyboard')
            except:
                pass
            self.Destroy()
        else:
            log_info('Main Frame - Closing declined') 
    
    def queue_handler(self, queue, port, queues):
        global queue_processing
        motors = []
        motors.append(rmd.RMD(id=0x01, port=port))
        motors.append(rmd.RMD(id=0x02, port=port))
        motors.append(rmd.RMD(id=0x03, port=port))
        m_queues = queues
        cmd_queue = queue

        while self.ready:
            try:
                if queue_processing:
                    payload = cmd_queue.get(True, 0.001)
                    # payload = tuple(priority, (id, command, data))
                    # priorities:
                    #   0 - stop command
                    #   1 - setup commands
                    #   2 - control commands
                    priority = payload[0]
                    id, cmd, data = payload[1]
                    log_debug(f'CMD Queue - Running {cmd} on motor with id {id}, data = {data} (priority: {priority})')
                    try:
                        if cmd == 'QUEUE_STOP':
                            for motor in motors:
                                try:
                                    motor.stop()
                                except: 
                                    pass
                            while not cmd_queue.empty():
                                payload = cmd_queue.get_nowait()                            
                        if cmd == 'start':
                            ret = motors[id-1].start()
                        elif cmd == 'stop':
                            ret = motors[id-1].stop()
                        elif cmd == 'shutdown':
                            ret = motors[id-1].shutdown()
                        elif cmd == 'single_position_control':
                            angle, direction, speed = data
                            ret = motors[id-1].single_position_control(angle, direction, speed)
                        elif cmd == 'multi_position_control':
                            angle, speed = data
                            ret = motors[id-1].multi_position_control(angle, speed)
                        elif cmd == 'speed_control':
                            ret = motors[id-1].speed_control(data)
                        elif cmd == 'torque_control':
                            ret = motors[id-1].torque_control(data)
                        elif cmd == 'read_encoder':
                            ret = motors[id-1].read_encoder()
                            m_queues[id-1].put(ret)
                        elif cmd == 'read_multiloop_angle':
                            ret = motors[id-1].read_multiloop_angle()
                            m_queues[id-1].put(ret)
                        elif cmd == 'read_singleloop_angle':
                            ret = motors[id-1].read_singleloop_angle()
                            m_queues[id-1].put(ret)
                        elif cmd == 'read_pid':
                            ret = motors[id-1].read_pid()
                            m_queues[id-1].put(ret)
                        elif cmd == 'write_pid':
                            angleKp, angleKi, speedKp, speedKi, torqueKp, torqueKi = data
                            ret = motors[id-1].write_pid(angleKp, angleKi, speedKp, speedKi, torqueKp, torqueKi)
                            m_queues[id-1].put(ret)
                        elif cmd == 'set_encoder_offset':
                            ret = motors[id-1].set_encoder_offset(data)
                            m_queues[id-1].put(ret)
                        elif cmd == 'read_motor_voltage_and_temperature':
                            ret = motors[id-1].read_motor_voltage_and_temperature()
                            m_queues[id-1].put(ret)
                        elif cmd == 'read_motor_status':
                            ret = motors[id-1].read_motor_status()
                            m_queues[id-1].put(ret)
                        elif cmd == 'read_motor_current':
                            ret = motors[id-1].read_motor_current()
                            m_queues[id-1].put(ret)
                        elif cmd == 'clear_error':
                            ret = motors[id-1].clear_error()
                        else:
                            log_warning('CMD Queue - Wrong command')
                            ret = -1
                    except Exception as e:
                        log_warning(f'CMD Queue - Could not execute command {cmd} on motor {id} because {e}')
                        continue
                else:
                    for motor in motors:
                        ret = motor.stop()
                    while not cmd_queue.empty():
                        cmd_queue.get_nowait()
                    queue_processing = True
            except:
                pass
        log_info('CMD Queue - Clearing command queue')
        while not cmd_queue.empty():
            cmd_queue.get_nowait()
        log_debug('CMD Queue - Leaving queue handler task')

    class RMDControls():
        def __init__(self, id=None, port=None, name='', cmd_queue=None, ret_queue=None):
            # Control
            self.id = id
            self.name = name
            self.portname = port
            self.freq = 10
            self.speed = 900
            self.single_angle = 0
            self.multi_angle = 0
            self.auto_mode = False
            self.thread = None

            # PID
            self.position_pid = [100, 100]
            self.speed_pid = [100, 50]
            self.torque_pid = [50, 50]

            # Status
            self.actual_single_loop_angle = 0
            self.actual_multi_loop_angle = 0
            self.actual_encoder_value = 0
            self.actual_encoder_offset = 0
            self.actual_voltage = 0
            self.actual_temperature = 0

            # queue
            self.cmd_queue = cmd_queue
            self.ret_queue = ret_queue
            global queue_processing
            queue_processing = True

            # zero position
            self.cmd_queue.put(item=(1, (self.id, 'read_multiloop_angle', None)))
            self.zero_position = self.ret_queue.get()
            log_info(f'Motor {self.name} zero position is {self.zero_position}')
            log_debug(f'Created RMDControls instance for id: {self.id}, name {self.name} on port {self.portname}. Zero is {self.zero_position}')

        def onStart(self, event):
            try:
                log_info(f'Starting motor {self.name} on port {self.portname}')
                self.cmd_queue.put((2, (self.id, 'start', None)))
            except Exception as e:
                log_error(f'Could not start motor {self.name} on port {self.portname} because {e}')

        def onStop(self, event):
            global queue_processing
            try:
                log_info(f'Stopping motor {self.name} on port {self.portname}')
                self._stop_thread_if_any()
                # self._back_to_zero()
                queue_processing = False
                self.cmd_queue.put(item=(0, (self.id, 'QUEUE_STOP', None)))
                time.sleep(0.2)
                self.cmd_queue.put(item=(2, (self.id, 'multi_position_control', (self.zero_position, 200))))
                time.sleep(0.2)
                self.cmd_queue.put((2, (self.id, 'stop', None)))
            except Exception as e:
                log_error(f'Could not stop motor {self.name} on port {self.portname} because {e}')

        def onShutdown(self, event):
            global queue_processing
            try:
                log_info(f'Shutting down motor {self.name} on port {self.portname}')
                self._stop_thread_if_any()
                self.cmd_queue.put(item=(0, (self.id, 'QUEUE_STOP', None)))
                time.sleep(0.2)
                queue_processing = False
                self.cmd_queue.put((2, (self.id, 'shutdown', None)))
            except Exception as e:
                log_error(f'Could not shutdown motor {self.name} on port {self.portname} because {e}')

        def onAuto(self, event):
            try:
                log_info(f'Starting thread with automatic move for motor: {self.name} on port: {self.portname}, freq: {self.freq} Hz')
                self._stop_thread_if_any()
                #self._back_to_zero()
                single_move_time = self.freq_to_sec(self.freq)
                self.auto_mode = True
                self.thread = threading.Thread(target=self._auto_move, args=(single_move_time,))
                self.thread.start()
            except Exception as e:
                self.auto_mode = False
                log_error(f'Could not start automatic move sequence for motor {self.name} on port: {self.portname} because {e}')

        def onFreqChange(self, event):
            try:
                source = wx.Event.GetEventObject(event)
                self.freq = source.GetValue()
                log_info(f'Changed freq of motor {self.name} on port: {self.portname} to {self.freq} Hz')
            except Exception as e:
                log_error(f'Could not change frequency value for motor {self.name} on port: {self.portname} because {e}')

        def onSpeedChange(self, event):
            try:
                source = wx.Event.GetEventObject(event)
                self.speed = source.GetValue()*100
                log_info(f'Changed speed of motor {self.name} on port: {self.portname} to {self.speed} deg/s')
            except Exception as e:
                log_error(f'Could not change speed value for motor {self.name} on port: {self.portname} because {e}')

        def onPosKpChange(self, event):
            try:
                source = wx.Event.GetEventObject(event)
                self.position_pid[0] = source.GetValue()
                log_info(f'Changed Kp of position of motor {self.name} on port: {self.portname} to {self.position_pid[0]}')
            except Exception as e:
                log_error(f'Could not change Kp of position for motor on port: {self.portname}({self.name}) because {e}')

        def onPosKiChange(self, event):
            try:
                source = wx.Event.GetEventObject(event)
                self.position_pid[1] = source.GetValue()
                log_info(f'Changed Ki of position of motor {self.name} on port: {self.portname} to {self.position_pid[1]}')
            except Exception as e:
                log_error(f'Could not change Ki of position for motor on port: {self.portname}({self.name}) because {e}')

        def onSpeedKpChange(self, event):
            try:
                source = wx.Event.GetEventObject(event)
                self.speed_pid[0] = source.GetValue()
                log_info(f'Changed Kp of speed of motor {self.name} on port: {self.portname} to {self.speed_pid[0]}')
            except Exception as e:
                log_error(f'Could not change Kp of speed for motor on port: {self.portname}({self.name}) because {e}')

        def onSpeedKiChange(self, event):
            try:
                source = wx.Event.GetEventObject(event)
                self.speed_pid[1] = source.GetValue()
                log_info(f'Changed Ki of speed of motor {self.name} on port: {self.portname} to {self.speed_pid[1]}')
            except Exception as e:
                log_error(f'Could not change Ki of speed for motor on port: {self.portname}({self.name}) because {e}')
        
        def onTorqueKpChange(self, event):
            try:
                source = wx.Event.GetEventObject(event)
                self.torque_pid[0] = source.GetValue()
                log_info(f'Changed Kp of torque of motor {self.name} on port: {self.portname} to {self.torque_pid[0]}')
            except Exception as e:
                log_error(f'Could not change Kp of torque for motor on port: {self.portname}({self.name}) because {e}')

        def onTorqueKiChange(self, event):
            try:
                log_info(f'Changed Ki of torque of motor {self.name} on port: {self.portname} to {self.torque_pid[1]}')
                source = wx.Event.GetEventObject(event)
                self.torque_pid[1] = source.GetValue()
            except Exception as e:
                log_error(f'Could not change Ki of torque for motor on port: {self.portname}({self.name}) because {e}')
        
        def set_pid(self):
            try:
                self.cmd_queue.put((2, (self.id, 'write_pid', (self.position_pid[0], self.position_pid[1],
                                self.speed_pid[0], self.speed_pid[1],
                                self.torque_pid[0], self.torque_pid[1]))))
                ret = self.ret_queue.get()
                if ret:
                    log_info(f'Changed PID controller values of motor {self.name} on port: {self.portname}')
                else:
                    log_error(f'Could not change PID controller values for motor {self.name} on port: {self.portname}')
            except Exception as e:
                log_error(f'Could not change PID controller values for motor {self.name} on port: {self.portname} because {e}')

        def get_pid(self):
            try:
                log_info(f'Downloaded PID controller values of motor {self.name} on port: {self.portname}')
                self.cmd_queue.put((2, (self.id, 'read_pid', None)))
                try:
                    pid = self.ret_queue.get(timeout=0.01)
                    # pid = self.read_pid()
                    self.position_pid[0] = pid[0]
                    self.position_pid[1] = pid[1]
                    self.speed_pid[0] = pid[2]
                    self.speed_pid[1] = pid[3]
                    self.torque_pid[0] = pid[4]
                    self.torque_pid[1] = pid[5]
                    return True
                except Exception as e:
                    log_error(f'Could not download PID of motor {self.name} because {e}')      
            except Exception as e:
                log_error(f'Could not get PID controller values for motor {self.name} on port: {self.portname} because {e}')
                return False

        def onEncoderOffsetChange(self, event):
            try:
                log_info(f'Changed encoder offset of motor {self.name} on port: {self.portname} to {self.actual_encoder_offset}')
                source = wx.Event.GetEventObject(event)
                self.actual_encoder_offset = source.GetValue()
            except Exception as e:
                log_error(f'Could not change encoder offset value for motor {self.name} on port: {self.portname} because {e}')

        def update_offset(self):
            try:
                log_info(f'Updated encoder offset value of motor {self.name} on port: {self.portname}')
                self.cmd_queue.put((2, (self.id, 'set_encoder_offset', self.actual_encoder_offset)))
                # self.set_encoder_offset(self.actual_encoder_offset)
            except Exception as e:
                log_error(f'Could not update encoder offset value for motor {self.name} on port: {self.portname} because {e}')

        def update_motor_status(self):
            try:
                log_info(f'Updated status of motor {self.name} on port: {self.portname}')
                self.cmd_queue.put((1, (self.id, 'read_motor_voltage_and_temperature', None)))
                self.cmd_queue.put((1, (self.id, 'read_encoder', None)))
                self.cmd_queue.put((1, (self.id, 'read_singleloop_angle', None)))
                self.cmd_queue.put((1, (self.id, 'read_multiloop_angle', None)))

                try:
                    ret = self.ret_queue.get(timeout=0.01)
                    self.actual_temperature, self.actual_voltage, lv, lt = ret
                    ret = self.ret_queue.get(timeout=0.01)
                    self.actual_encoder_value, enc_raw, self.actual_encoder_offset = ret
                    ret = self.ret_queue.get(timeout=0.01)
                    self.actual_single_loop_angle = ret
                    ret = self.ret_queue.get(timeout=0.01)
                    self.actual_multi_loop_angle = ret
                except Exception as e:
                    log_error(f'Could not get status for motor {self.name} because {e}')
                # self.actual_temperature, self.actual_voltage, lv, lt = self.read_motor_voltage_and_temperature()
                # self.actual_encoder_value, enc_raw, self.actual_encoder_offset = self.read_encoder()
                # self.actual_single_loop_angle = self.read_singleloop_angle()
                # self.actual_multi_loop_angle = self.read_multiloop_angle()
            except Exception as e:
                log_error(f'Could not update status for motor {self.name} on port: {self.portname} because {e}')

        def _stop_thread_if_any(self):
            self.auto_mode = False
            if self.thread:
                log_warning(f'Stopping thread for motor {self.name} on port: {self.portname}')
                self.thread.join(0.01)
            self.thread = None

        def _auto_move(self, move_time):
            try:
                if self.speed != 0:
                    default_speed = round(abs(self.speed))
                else:
                    default_speed = 900
                default_freq = round(1/move_time)
                log_info(f'Starting automatic move sequence for motor {self.name} on port: {self.portname} and frequency {self.freq} Hz, speed {default_speed}')
                self.cmd_queue.put(item=(2, (self.id, 'multi_position_control', (self.zero_position, 200))))
                time.sleep(move_time/4)
                while self.auto_mode:
                    if self.speed != default_speed:
                        default_speed = round(abs(self.speed))
                        log_info(f'Auto mode {self.name} - Auto move speed changed to {default_speed}')
                    if self.freq != default_freq:
                        move_time = self.freq_to_sec(self.freq)
                        default_freq = self.freq
                        log_info(f'Auto mode {self.name} - Auto move time changed to {move_time} (freq changed to {self.freq} Hz)')
                    self.cmd_queue.put(item=(2, (self.id, 'multi_position_control', (-1*default_freq*default_speed*3/4, default_speed))))
                    time.sleep(move_time*3/4)
                    self.cmd_queue.put(item=(2, (self.id, 'multi_position_control', (-1*default_freq*default_speed*7/8, default_speed))))
                    time.sleep(move_time/8)
                    self.cmd_queue.put(item=(2, (self.id, 'multi_position_control', (-1*default_freq*default_speed, default_speed))))
                    time.sleep(move_time/8)

                    self.cmd_queue.put(item=(2, (self.id, 'multi_position_control', (default_freq*default_speed*3/4, default_speed))))
                    time.sleep(move_time*3/4)
                    self.cmd_queue.put(item=(2, (self.id, 'multi_position_control', (default_freq*default_speed*7/8, default_speed))))
                    time.sleep(move_time/8)
                    self.cmd_queue.put(item=(2, (self.id, 'multi_position_control', (default_freq*default_speed, default_speed))))
                    time.sleep(move_time/8)
            
            except Exception as e:
                log_error(f'Auto mode {self.name} - Automatic move sequence stopped because {e}')
                self.auto_mode = False
            
            self.cmd_queue.put(item=(self.id, 'multi_position_control', (self.zero_position, 200)))
            log_warning(f'Auto mode {self.name} -  Leaving automatic move sequence thread of motor {self.name} on port: {self.portname}')

        # def _auto_move(self, move_time):
        #     try:
        #         if self.speed != 0:
        #             default_speed = round(abs(self.speed))
        #         else:
        #             default_speed = 900
        #         default_freq = round(1/move_time)
        #         log_info(f'Starting automatic move sequence for motor {self.name} on port: {self.portname} and frequency {self.freq} Hz, speed {default_speed}')
        #         #default_speed = abs(self.speed)
        #         # self.speed_control(default_speed)
        #         # self.cmd_queue.put(item=(self.id, 'speed_control', default_speed))
        #         # time.sleep(move_time)
        #         # # self.speed_control(0)
        #         # self.cmd_queue.put(item=(self.id, 'speed_control', 0))
        #         self.cmd_queue.put(item=(self.id, 'multi_position_control', (self.zero_position, 200)))
        #         time.sleep(move_time/4)
        #         while self.auto_mode:
        #             if self.speed != default_speed:
        #                 default_speed = round(abs(self.speed))
        #                 log_info(f'Auto mode {self.name} - Auto move speed changed to {default_speed}')
        #             if self.freq != default_freq:
        #                 move_time = self.freq_to_sec(self.freq)
        #                 default_freq = self.freq
        #                 log_info(f'Auto mode {self.name} - Auto move time changed to {move_time} (freq changed to {self.freq} Hz)')
        #             # self.speed_control(-default_speed)
        #             self.cmd_queue.put(item=(self.id, 'speed_control', -default_speed))
        #             time.sleep(move_time*3/4)
        #             # self.speed_control(-default_speed*0.25)
        #             self.cmd_queue.put(item=(self.id, 'speed_control', -default_speed*0.25))
        #             time.sleep(move_time/8)
        #             # self.speed_control(0)
        #             self.cmd_queue.put(item=(self.id, 'speed_control', 0))
        #             time.sleep(move_time/8)

        #             # self.speed_control(default_speed)
        #             self.cmd_queue.put(item=(self.id, 'speed_control', default_speed))
        #             time.sleep(move_time*3/4)
        #             # self.speed_control(default_speed*0.25)
        #             self.cmd_queue.put(item=(self.id, 'speed_control', default_speed*0.25))
        #             time.sleep(move_time/8)
        #             # self.speed_control(0)
        #             self.cmd_queue.put(item=(self.id, 'speed_control', 0))
        #             time.sleep(move_time/8)
            
        #     except Exception as e:
        #         log_error(f'Auto mode {self.name} - Automatic move sequence stopped because {e}')
        #         self.auto_mode = False
            
        #     self.cmd_queue.put(item=(self.id, 'multi_position_control', (self.zero_position, 200)))
        #     log_warning(f'Auto mode {self.name} -  Leaving automatic move sequence thread of motor {self.name} on port: {self.portname}')

        @staticmethod
        def freq_to_sec(frequency):
            sec = round(1/frequency,5)
            log_debug(f'Converting {frequency} Hz into {sec} seconds')
            return sec

class ControlTab(MyPanelWithFont):
    def __init__(self, parent, motors=None):
        super().__init__(parent)
        log_debug('Control - Creating control tab')

        self.motors = motors
        box = wx.BoxSizer(wx.VERTICAL)
        
        log_debug('Control - Setting auto start button')
        button_full_auto = wx.Button(self, wx.ID_ANY, 'Auto Start', pos=(100,10), size=(100,23))
        box.Add(button_full_auto, 0, wx.ALL)
        self.Bind(wx.EVT_BUTTON, self.onAutoStart, button_full_auto)

        log_debug('Control - Setting auto stop button')
        button_full_stop = wx.Button(self, wx.ID_ANY, 'Auto Stop', pos=(240,10), size=(100,23))
        box.Add(button_full_stop, 0, wx.ALL)
        self.Bind(wx.EVT_BUTTON, self.onAutoStop, button_full_stop)

        log_debug('Control - Setting control for motor 1')   
        wx.StaticBox(self, wx.ID_ANY, 'Motor 1', (3,35), (145,175))
        self.motor_setup(box, motors[0], (5,35))

        log_debug('Control - Setting control for motor 2')
        wx.StaticBox(self, wx.ID_ANY, 'Motor 2', (153,35), (145,175))    
        self.motor_setup(box, motors[1], (155,35))

        log_debug('Control - Setting control for motor 3')
        wx.StaticBox(self, wx.ID_ANY, 'Motor 3', (303,35), (145,175))    
        self.motor_setup(box, motors[2], (305,35))

    def motor_setup(self, box, motor, start_pos):
        x, y = start_pos
        x_first_col = x+5
        x_second_col = x+75
        y_base = 20
        y_increment = 25
        log_debug('Control - Setting buttons')
        m_button_start = wx.Button(self, wx.ID_ANY, 'Start', pos=(x_first_col, y+y_base), size=(60,23))
        m_button_stop = wx.Button(self, wx.ID_ANY, 'Stop', pos=(x_second_col, y+y_base), size=(60,23))
        m_button_shut = wx.Button(self, wx.ID_ANY, 'Shut', pos=(x_first_col, y+y_base+y_increment), size=(60,23))
        m_button_auto = wx.Button(self, wx.ID_ANY, 'Auto Mode', pos=(x_second_col, y+y_base+y_increment), size=(60,23))
        
        wx.StaticText(self, wx.ID_ANY, 'Hz', pos=(x+122, y+y_base+y_increment*2+8))
        wx.StaticText(self, wx.ID_ANY, 'Max speed:', pos=(x_first_col, y+y_base+y_increment*3+8))
        wx.StaticText(self, wx.ID_ANY, 'x 100', pos=(x+115, y+y_base+y_increment*4+4))
        
        log_debug('Control - Setting spin controllers')
        m_input_freq = wx.SpinCtrl(self, wx.ID_ANY, pos=(x_first_col+5, y+y_base+y_increment*2+4), size=(100,23), min=0, max=35, initial=10)
        m_input_speed = wx.SpinCtrl(self, wx.ID_ANY, pos=(x_first_col+5,y+y_base+y_increment*4), size=(100,23), min=0, max=72, initial=9)
        
        log_debug('Control - Adding buttons and controllers to panel')
        box.Add(m_button_start, 0, wx.ALL)
        box.Add(m_button_stop, 0, wx.ALL)
        box.Add(m_button_shut, 0, wx.ALL)
        box.Add(m_button_auto, 0, wx.ALL)
        box.Add(m_input_freq, 0, wx.ALL)
        box.Add(m_input_speed, 0, wx.ALL)

        log_debug('Control - Binding controls to callbacks')
        self.Bind(wx.EVT_BUTTON, motor.onStart, m_button_start)
        self.Bind(wx.EVT_BUTTON, motor.onStop, m_button_stop)
        self.Bind(wx.EVT_BUTTON, motor.onShutdown, m_button_shut)
        self.Bind(wx.EVT_BUTTON, motor.onAuto, m_button_auto)
        self.Bind(wx.EVT_SPINCTRL, motor.onFreqChange, m_input_freq)
        self.Bind(wx.EVT_SPINCTRL, motor.onSpeedChange, m_input_speed)

    def onAutoStart(self, event):
        log_info('Control - Starting all motors in auto mode')
        for motor in self.motors:
            motor.onAuto(event)

    def onAutoStop(self, event):
        log_info('Control - Stopping all motors from auto mode')
        for motor in self.motors:
            motor.auto_mode = False

class PIDTab(MyPanelWithFont):
    def __init__(self, parent, motors):
        super().__init__(parent)
        self.motors = motors

        box = wx.BoxSizer(wx.VERTICAL)

        log_debug('PID - Setting upload button')
        button_upload = wx.Button(self, wx.ID_ANY, 'Upload', pos=(300,10), size=(100,23))
        box.Add(button_upload, 0, wx.ALL)
        self.Bind(wx.EVT_BUTTON, self.onUpload, button_upload)

        log_debug('PID - Setting update button')
        button_update = wx.Button(self, wx.ID_ANY, 'Update', pos=(180,10), size=(100,23))
        box.Add(button_update, 0, wx.ALL)
        self.Bind(wx.EVT_BUTTON, self.onUpdate, button_update)

        log_debug('PID - Setting download button')
        button_download = wx.Button(self, wx.ID_ANY, 'Download', pos=(60,10), size=(100,23))
        box.Add(button_download, 0, wx.ALL)
        self.Bind(wx.EVT_BUTTON, self.onDownload, button_download)
        
        self.pos_kp =list()
        self.pos_ki = list()
        self.sp_kp = list()
        self.sp_ki = list()
        self.trq_kp = list()
        self.trq_ki = list()

        log_debug('PID - Setting control for motor 1')  
        wx.StaticBox(self, wx.ID_ANY, 'Motor 1', (20,35), (130,175))
        self.motor_setup(box, motors[0], (32,35))

        log_debug('PID - Setting control for motor 2')  
        wx.StaticBox(self, wx.ID_ANY, 'Motor 2', (160,35), (130,175))
        self.motor_setup(box, motors[0], (172,35))

        log_debug('PID - Setting control for motor 3')  
        wx.StaticBox(self, wx.ID_ANY, 'Motor 3', (300,35), (130,175))
        self.motor_setup(box, motors[0], (312,35))

    def motor_setup(self, box, motor, start_pos):
        x, y = start_pos
        x_first_col = x+10
        x_second_col = x+40
        y_base = 15
        y_increment = 17
        log_debug('PID - Setting up text fields')
        wx.StaticText(self, wx.ID_ANY, 'Position:', pos=(x, y+y_base))
        wx.StaticText(self, wx.ID_ANY, 'Kp: ', pos=(x_first_col, y+y_base+y_increment))
        wx.StaticText(self, wx.ID_ANY, 'Ki: ', pos=(x_first_col, y+y_base+y_increment*2))

        wx.StaticText(self, wx.ID_ANY, 'Speed:', pos=(x, y+y_base+y_increment*3))
        wx.StaticText(self, wx.ID_ANY, 'Kp: ', pos=(x_first_col, y+y_base+y_increment*4))
        wx.StaticText(self, wx.ID_ANY, 'Ki: ', pos=(x_first_col, y+y_base+y_increment*5))

        wx.StaticText(self, wx.ID_ANY, 'Torque:', pos=(x, y+y_base+y_increment*6))
        wx.StaticText(self, wx.ID_ANY, 'Kp: ', pos=(x_first_col, y+y_base+y_increment*7))
        wx.StaticText(self, wx.ID_ANY, 'Ki: ', pos=(x_first_col, y+y_base+y_increment*8))

        log_debug('PID - Setting up PID values control fields')
        self.pos_kp.append(wx.SpinCtrl(self, wx.ID_ANY, str(motor.position_pid[0]), pos=(x_second_col, y+y_base+y_increment), size=(55, y_increment),
                                    min=1, max=1000, initial=motor.position_pid[0]))
        self.pos_ki.append(wx.SpinCtrl(self, wx.ID_ANY, str(motor.position_pid[1]), pos=(x_second_col, y+y_base+y_increment*2), size=(55, y_increment),
                            min=0, max=1000, initial=motor.position_pid[1]))

        self.sp_kp.append(wx.SpinCtrl(self, wx.ID_ANY, str(motor.speed_pid[0]), pos=(x_second_col, y+y_base+y_increment*4), size=(55, y_increment),
                                    min=1, max=1000, initial=motor.speed_pid[0]))
        self.sp_ki.append(wx.SpinCtrl(self, wx.ID_ANY, str(motor.speed_pid[1]), pos=(x_second_col, y+y_base+y_increment*5), size=(55, y_increment),
                            min=0, max=1000, initial=motor.speed_pid[1]))

        self.trq_kp.append(wx.SpinCtrl(self, wx.ID_ANY, str(motor.torque_pid[0]), pos=(x_second_col, y+y_base+y_increment*7), size=(55, y_increment),
                                    min=1, max=1000, initial=motor.torque_pid[0]))
        self.trq_ki.append(wx.SpinCtrl(self, wx.ID_ANY, str(motor.torque_pid[1]), pos=(x_second_col, y+y_base+y_increment*8), size=(55, y_increment),
                            min=0, max=1000, initial=motor.torque_pid[1]))

        self.Bind(wx.EVT_SPINCTRL, motor.onPosKpChange, self.pos_kp[-1])
        self.Bind(wx.EVT_SPINCTRL, motor.onPosKiChange, self.pos_ki[-1])
        self.Bind(wx.EVT_SPINCTRL, motor.onSpeedKpChange, self.sp_kp[-1])
        self.Bind(wx.EVT_SPINCTRL, motor.onSpeedKiChange, self.sp_ki[-1])
        self.Bind(wx.EVT_SPINCTRL, motor.onTorqueKpChange, self.trq_kp[-1])
        self.Bind(wx.EVT_SPINCTRL, motor.onTorqueKiChange, self.trq_ki[-1])

    def onUpdate(self, event):
        log_info('PID - Updating PID settings')
        self.onUpload(wx.EVT_BUTTON)
        self.onDownload(wx.EVT_BUTTON)    

    def onUpload(self, event):
        log_info('PID - Uploading PID settings')
        for motor in self.motors:
            motor.set_pid()

    def onDownload(self, event):
        log_info('PID - Downloading PID settings')
        for idx, motor in enumerate(self.motors):
            if motor.get_pid():
                self.pos_kp[idx].SetValue(motor.position_pid[0])
                self.pos_ki[idx].SetValue(motor.position_pid[1])
                self.sp_kp[idx].SetValue(motor.speed_pid[0])
                self.sp_ki[idx].SetValue(motor.speed_pid[1])
                self.trq_kp[idx].SetValue(motor.torque_pid[0])
                self.trq_ki[idx].SetValue(motor.torque_pid[1])

class StatusTab(MyPanelWithFont):
    def __init__(self, parent, motors):
        super().__init__(parent)
        box = wx.BoxSizer(wx.VERTICAL)

        self.motors = motors

        self.angle_singles = list()
        self.angle_multis = list()
        self.encoder_values = list()
        self.encoder_offsets = list()
        self.voltages = list()
        self.temperatures = list()

        log_debug('Status - Setting upload button')
        button_upload = wx.Button(self, wx.ID_ANY, 'Upload', pos=(300,10), size=(100,23))
        box.Add(button_upload, 0, wx.ALL)
        self.Bind(wx.EVT_BUTTON, self.onUpload, button_upload)

        log_debug('Status - Setting update button')
        button_update = wx.Button(self, wx.ID_ANY, 'Update', pos=(180,10), size=(100,23))
        box.Add(button_update, 0, wx.ALL)
        self.Bind(wx.EVT_BUTTON, self.onUpdate, button_update)

        log_debug('Status - Setting download button')
        button_download = wx.Button(self, wx.ID_ANY, 'Download', pos=(60,10), size=(100,23))
        box.Add(button_download, 0, wx.ALL)
        self.Bind(wx.EVT_BUTTON, self.onDownload, button_download)
        
        log_debug('Status - Setting status box for motor 1')  
        wx.StaticBox(self, wx.ID_ANY, 'Motor 1', (20,35), (130,175))
        self.motor_setup(box, motors[0], (32,35))

        log_debug('Status - Setting status box for motor 2')  
        wx.StaticBox(self, wx.ID_ANY, 'Motor 2', (160,35), (130,175))
        self.motor_setup(box, motors[0], (172,35))

        log_debug('Status - Setting status box for motor 3')  
        wx.StaticBox(self, wx.ID_ANY, 'Motor 3', (300,35), (130,175))
        self.motor_setup(box, motors[0], (312,35))

    def motor_setup(self, box, motor, start_pos):
        y_base = 15
        y_increment = 17
        x_increment = 15
        x, y = start_pos
        log_debug('Status - Setting text fields')
        wx.StaticText(self, wx.ID_ANY, 'Angle:', pos=(x,y+y_base))
        wx.StaticText(self, wx.ID_ANY, 'Single: ', pos=(x+x_increment,y+y_base+y_increment))
        wx.StaticText(self, wx.ID_ANY, 'Multi: ', pos=(x+x_increment,y+y_base+y_increment*2))

        wx.StaticText(self, wx.ID_ANY, 'Encoder:', pos=(x,y+y_base+y_increment*3))
        wx.StaticText(self, wx.ID_ANY, 'Value: ', pos=(x+x_increment,y+y_base+y_increment*4))
        wx.StaticText(self, wx.ID_ANY, 'Offset: ', pos=(x+x_increment,y+y_base+y_increment*5))

        wx.StaticText(self, wx.ID_ANY, 'Status:', pos=(x,y+y_base+y_increment*6))
        wx.StaticText(self, wx.ID_ANY, 'Voltage: ', pos=(x+x_increment,y+y_base+y_increment*7))
        wx.StaticText(self, wx.ID_ANY, 'Temp: ', pos=(x+x_increment,y+y_base+y_increment*8))

        self.angle_singles.append(wx.StaticText(self, wx.ID_ANY, str(motor.actual_single_loop_angle), pos=(x+x_increment*4, y+y_base+y_increment)))
        self.angle_multis.append(wx.StaticText(self, wx.ID_ANY, str(motor.actual_multi_loop_angle), pos=(x+x_increment*4, y+y_base+y_increment*2)))

        self.encoder_values.append(wx.StaticText(self, wx.ID_ANY, str(motor.actual_encoder_value), pos=(x+x_increment*4, y+y_base+y_increment*4)))
        self.encoder_offsets.append(wx.SpinCtrl(self, wx.ID_ANY, str(motor.actual_encoder_offset), pos=(x+x_increment*4, y+y_base+y_increment*5), size=(55,20),
                                    min=0, max=100000, initial=motor.actual_encoder_offset))

        self.voltages.append(wx.StaticText(self, wx.ID_ANY, str(motor.actual_voltage), pos=(x+x_increment*4, y+y_base+y_increment*7)))
        self.temperatures.append(wx.StaticText(self, wx.ID_ANY, str(motor.actual_temperature), pos=(x+x_increment*4, y+y_base+y_increment*8)))

        self.Bind(wx.EVT_BUTTON, motor.onEncoderOffsetChange, self.encoder_offsets[-1])

    def onUpdate(self, event):
        log_info('Status - Updating status of motors')
        self.onUpload(wx.EVT_BUTTON)
        self.onDownload(wx.EVT_BUTTON)

    def onUpload(self, event):
        log_info('Status - Uploading encoder offsets to motors')
        for idx, motor in enumerate(self.motors):
            motor.update_offset()

    def onDownload(self, event):
        log_info('Status - Updating status of motors')
        for idx, motor in enumerate(self.motors):
            motor.update_motor_status()
            self.angle_singles[idx].SetLabel(str(motor.actual_single_loop_angle))
            self.angle_multis[idx].SetLabel(str(motor.actual_multi_loop_angle))
            self.encoder_values[idx].SetLabel(str(motor.actual_encoder_value))
            self.encoder_offsets[idx].SetValue(motor.actual_encoder_offset)
            self.voltages[idx].SetLabel(str(motor.actual_voltage))
            self.temperatures[idx].SetLabel(str(motor.actual_temperature))

class OptionsTab(MyPanelWithFont):
    def __init__(self, parent):
        super().__init__(parent)
        log_debug('Options - Creating options tab')

        box = wx.BoxSizer(wx.VERTICAL)
        
        log_debug('Options - Creating buttons on options tab')
        turnoff_button = wx.Button(self, wx.ID_ANY, 'Turn off', pos=(50, 70), size=(100,40))
        restart_button = wx.Button(self, wx.ID_ANY, 'Restart', pos=(180, 70), size=(100,40))
        about_button = wx.Button(self, wx.ID_ANY, 'About', pos=(310, 70), size=(100,40))

        box.Add(turnoff_button, 0, wx.ALL)    
        box.Add(restart_button, 0, wx.ALL)
        box.Add(about_button, 0, wx.ALL)

        self.Bind(wx.EVT_BUTTON, self.onRestart, restart_button)
        self.Bind(wx.EVT_BUTTON, self.onTurnOff, turnoff_button)
        self.Bind(wx.EVT_BUTTON, self.onAbout, about_button)

        self.start_time = time.time()
        self.time_running = '00h:00m:00s'
        wx.StaticText(self, wx.ID_ANY, 'Working time: ', pos=(50, 20))
        self.timer = wx.StaticText(self, wx.ID_ANY, self.time_running, pos=(120, 20))
        thread = threading.Thread(target=self.update_timer)
        thread.start()

    def onRestart(self, event):
        log_info('Restart button clicked')
        dialog_box = wx.MessageDialog(self, "Device will restart", 'Restart', wx.OK|wx.CANCEL|wx.ICON_QUESTION)
        result = dialog_box.ShowModal()
        dialog_box.Destroy()
        if result == wx.ID_OK:
            log_info('Restart confirmed') 
            os.system(f'sudo reboot ; echo {Frame.PASSWORD}')
        else:
            log_info('Restart declined')

    def onTurnOff(self, event):
        log_info('Turn off button clicked')
        dialog_box = wx.MessageDialog(self, "Device will turn off", 'Turn Off', wx.OK|wx.CANCEL|wx.ICON_QUESTION)
        result = dialog_box.ShowModal()
        dialog_box.Destroy()
        if result == wx.ID_OK:
            log_info('Turning off confirmed')
            log_info('Adios') 
            os.system(f'sudo shutdown -h now ; echo {Frame.PASSWORD}')
        else:
            log_info('Turning off declined') 

    def onAbout(self, event):
        log_info('About button clicked')
        msg = "Kontroler fotelika imitującego środowisko jazdy samochodem v1.0\n" \
              "Praca magisterska, inż. Mateusz Płatek\n" \
              "Kraków, 2021\n" 
        dialog_window = wx.MessageBox(msg, 'Informations')

    def update_timer(self):
        time.sleep(4)
        while True:
            elapsed = time.time() - self.start_time
            h = floor(elapsed/3600)
            m = floor(elapsed/60)
            s = round(elapsed%60)
            time_running = f'{h:02d}h:{m:02d}m:{s:02d}s'
            wx.CallAfter(self.timer.SetLabel, time_running)
            time.sleep(1)

class EnvTab(MyPanelWithFont):
    AUDIO_FILE = '/home/pi/RMD/car_audio.wav'

    def __init__(self, parent):
        super().__init__(parent)
        log_debug('ENV - Creating environment tab')

        box = wx.BoxSizer(wx.VERTICAL)
        wx.StaticBox(self, wx.ID_ANY, '', (70,40), (340,120))

        self.sound_check = wx.CheckBox(self, wx.ID_ANY, label="Sound environment enable", pos=(150,80))
        self.light_check = wx.CheckBox(self, wx.ID_ANY, label="Light environment enable", pos=(150,110))

        self.Bind(wx.EVT_CHECKBOX, self.OnSoundChange, self.sound_check)
        self.Bind(wx.EVT_CHECKBOX, self.OnLightChange, self.light_check)

        self.sound_enable = False
        self.sound_player = vlc.MediaPlayer(EnvTab.AUDIO_FILE)
        self.sound_process = None

    def OnSoundChange(self, event):
        log_debug('ENV - Sound environment status changed')
        if self.sound_check.IsChecked():
            log_info('Sound - Turning on')
            self.sound_enable = True
            self.sound_process = threading.Thread(target=self._sound_player)
            self.sound_process.start()
        else:
            log_warning('Sound - Turning off')
            self.sound_enable = False
            self.sound_process.join()
            self.sound_process = None
            # self.sound_player.stop()

    def OnLightChange(self, event):
        log_debug('ENV - Light environment status changed')
        if self.light_check.IsChecked():
            # enable light on Raspi
            log_info('Light - Turning on')
        else:
            # disbale light on Raspi
            log_warning('Light - Turning off')

    def _sound_player(self):
        while self.sound_enable:
            if self.sound_player.is_playing():
                self.sound_player.play()
        self.sound_player.stop()

if __name__ == '__main__':
    sys.stdout = MyLogger()
    sys.stderr = sys.stdout
    
    log_info('Main - GUI start')
    app = wx.App(redirect=False)#True, filename='logfile.log')
    Frame('Car simulator controller').Show()
    app.MainLoop()