import serial
import serial.rs485
import time

READ_PID_PARAM                  = 0x30
WRITE_PID_PARAM_TO_RAM          = 0x31
WRITE_PID_PARAM_TO_ROM          = 0x32
READ_ACCELERATION               = 0x33
WRITE_ACCELERATION_TO_RAM       = 0x34
READ_ENCODER                    = 0x90
WRITE_ENCODER_AS_ZERO           = 0x91
WRITE_POSITION_TO_ROM_AS_ZERO   = 0x19
READ_MULTILOOP_ANGLE            = 0x92
READ_SINGLELOOP_ANGLE           = 0x94
READ_MOTOR_STATUS1_AND_ERROR    = 0x9A
CLEAR_MOTOR_ERROR               = 0x9B
READ_MOTOR_STATUS2              = 0x9C
READ_MOTOR_STATUS3              = 0x9D
MOTOR_SHUTDOWN                  = 0x80
MOTOR_STOP                      = 0x81
MOTOR_OPERATION                 = 0x88
OPEN_LOOP_CONTROL               = 0xA0
TORQUE_CONTROL                  = 0xA1
SPEED_CONTROL                   = 0xA2
MULTIPOSITION_CONTROL1          = 0xA3
MULTIPOSITION_CONTROL2          = 0xA4
SINGLEPOSITION_CONTROL1         = 0xA5
SINGLEPOSITION_CONTROL2         = 0xA6
INCREMENTAL_CONTROL1            = 0xA7
INCREMENTAL_CONTROL2            = 0xA8
READ_DRIVER_AND_MOTOR           = 0x12

ROTATION_CW                     = 0X00
ROTATION_CCW                    = 0X01

class RMD():
    _ID = 0x01
    _HEAD_BYTE = 0x3E
    RESPONSE_WAIT_TIME_SEC = 0.0005*2

    def __init__(self, id=None, port=None):
        """
        Args:
            id (int) : individual identificator for RMD motor, id >= 1
            port (str) : name of port (like 'COMX' or 'dev/ttyX')
        """
        if id:
            self.ID = id
        else:
            self.ID = RMD._ID
            RMD._ID += 0x01

        # self.port = serial.rs485.RS485(port=port)
        self.port = serial.Serial(port=port)
        # initial setup
        self.port.baudrate = 115200
        self.port.bytesize = serial.EIGHTBITS
        self.port.parity = serial.PARITY_NONE
        self.port.stopbits = serial.STOPBITS_ONE
        # enable RS485 mode
        self.port.rs485_mode = serial.rs485.RS485Settings()

    def start(self):
        """ Start RMD
        """
        try:
            if self.port.is_open:
                self.port.close()
            self.port.open()

            return self.send_data(MOTOR_OPERATION)
        except Exception as e:
            print(f'Could not open port because {e}')
            return False
        
    def stop(self):
        """ Stop RMD without clearing previously received control instructions

        Returns:
            (bool) : if command sent or not
        """
        return self.send_data(MOTOR_STOP)

    def shutdown(self):
        """ Shutdown RMD - stop motor and erase previous instructions
        """
        self.send_data(MOTOR_SHUTDOWN)
        self.port.close()
        return True

    def single_position_control(self, angle, direction, speed=None, angles=True):
        """ Single-position closed loop control with direction of the spin
        Set single value to get and hold

        Args:
            angle (int) : desired angle to stabilize in degress (or resolutions (deg*100) if angles=False), 0-359.99 deg
            direction (int) : ROTATION_CW or ROTATION_CCW
            speed (int) : maximum speed to move in degress/sec (or resolutions/sec (dps*100) if angles=False), 0-360 dps
            angles (bool) : if True, values passed to angle and speed are in degress (0-359.99) instead of resolutions (0-35999)
        Returns:
            (bool) : success
        """
        if angles:
            angle_bytes = bytearray.fromhex(self.int2hex(abs(angle*100)))
        else:
            angle_bytes = bytearray.fromhex(self.int2hex(abs(angle)))
        angle_bytes = self.adjust_len(angle_bytes, 2)
        if speed:
            if angles:
                if speed < 0:
                    speed_bytes = bytearray.fromhex(self.int2hex(speed*100, signed=True, nbits=4*8))
                else:
                    speed_bytes = bytearray.fromhex(self.int2hex(speed*100))
            else:
                if speed < 0:
                    speed_bytes = bytearray.fromhex(self.int2hex(speed, signed=True, nbits=4*8))
                else:
                    speed_bytes = bytearray.fromhex(self.int2hex(speed))
            speed_bytes = self.adjust_len(speed_bytes, 4)
            data = [direction, angle_bytes[1], angle_bytes[0], 0x00, 
                    speed_bytes[3], speed_bytes[2], speed_bytes[1], speed_bytes[0]]
            return self.send_data(SINGLEPOSITION_CONTROL2, data)
        else:
            data = [direction, angle_bytes[1], angle_bytes[0], 0x00]
            return self.send_data(SINGLEPOSITION_CONTROL1, data)

    def multi_position_control(self, angle, speed=None, angles=True):
        """ Multi-position closed loop control mode 
        Set single position to get and hold

        Args:
            angle (int) : desired angle to stabilize in degress (or resolutions (deg*100) if angles=False), 
            speed (int) : maximum speed to move in degress/sec (or resolutions/sec (dps*100) if angles=False), 0-360 dps
            angles (bool) : if True, values passed to angle and speed are in degress (0-359.99) instead of resolutions (0-35999)
        Returns:
            (bool) : success
        """
        if angles:
            if angle < 0:
                angle_bytes = bytearray.fromhex(self.int2hex(angle*100, signed=True, nbits=8*8))
            else:
                angle_bytes = bytearray.fromhex(self.int2hex(angle*100))
        else:
            if angle < 0:
                angle_bytes = bytearray.fromhex(self.int2hex(angle, signed=True, nbits=8*8))
            else:
                angle_bytes = bytearray.fromhex(self.int2hex(angle))
        angle_bytes = self.adjust_len(angle_bytes, 8)
        if speed:
            if angles:
                if speed < 0:
                    speed_bytes = bytearray.fromhex(self.int2hex(speed*100, signed=True, nbits=4*8))
                else:
                    speed_bytes = bytearray.fromhex(self.int2hex(speed*100))
            else:
                if speed < 0:
                    speed_bytes = bytearray.fromhex(self.int2hex(speed, signed=True, nbits=4*8))
                else:
                    speed_bytes = bytearray.fromhex(self.int2hex(speed))
            speed_bytes = self.adjust_len(speed_bytes, 4)
            
            data = [angle_bytes[7], angle_bytes[6], angle_bytes[5], angle_bytes[4], 
                    angle_bytes[3], angle_bytes[2], angle_bytes[1], angle_bytes[0], 
                    speed_bytes[3], speed_bytes[2], speed_bytes[1], speed_bytes[0]]
            return self.send_data(MULTIPOSITION_CONTROL2, data)
        else:
            data = [angle_bytes[7], angle_bytes[6], angle_bytes[5], angle_bytes[3], 
                    angle_bytes[3], angle_bytes[2], angle_bytes[1], angle_bytes[0]]
            return self.send_data(MULTIPOSITION_CONTROL1, data)

    def speed_control(self, speed, degrees=True):
        """ Speed closed-loop control mode with speed as desired value

        Args:
            speed (int) : maximum speed to move in degress/sec (or resolutions/sec (dps*100) if degrees=False), 0-360 dps
            direction (int) : 
            degress (bool) : if True, values passed are in degress per sec (0-359.99) instead of resolutions per sec (0-35999)
        Returns:
            (bool) : success
        """
        if degrees:
            if speed < 0:
                speed_bytes = bytearray.fromhex(self.int2hex(speed*100, signed=True, nbits=4*8))
            else:
                speed_bytes = bytearray.fromhex(self.int2hex(speed*100))
        else:
            if speed < 0:
                speed_bytes = bytearray.fromhex(self.int2hex(speed, signed=True, nbits=4*8))
            else:
                speed_bytes = bytearray.fromhex(self.int2hex(speed))
        speed_bytes = self.adjust_len(speed_bytes, 4)
        data = [speed_bytes[3], speed_bytes[2], speed_bytes[1], speed_bytes[0]]
        return self.send_data(SPEED_CONTROL, data)

    def torque_control(self, torque_current):
        """ Torque closed-loop control mode with torque current as desired value

        Args:
            torque current (float) : desired current Iq
        Returns:
            (bool) : success
        """
        if torque_current > 32:
            torque_current = 32
        elif torque_current < -32:
            torque_current = -32
        torque_value = round(torque_current / 64) #(33/2048))
        if torque_value >= 0:
            torque_bytes = self.int2hex(torque_value)
        else:
            torque_bytes = self.int2hex(torque_value, signed=True, nbits=2*8)
        print(f'{torque_value}, {torque_current}, {torque_bytes}')
        torque_bytes = self.adjust_len(torque_bytes, 2) 
        data = [torque_bytes[1], torque_bytes[0]]
        return self.send_data(TORQUE_CONTROL, data)
        
    def read_encoder(self):
        """ Read data from encoder

        Returns:
            (int) : original position minus offset
            (int) : original position
            (int) : offset - point taken as the 0 point of the motor angle
        """
        ret = self.get_data(READ_ENCODER)
        if ret:
            encoder = self.hex2int([ret[0], ret[1]])
            encoder_raw = self.hex2int([ret[2], ret[3]])
            encoder_offset = self.hex2int([ret[4], ret[5]])
            return encoder, encoder_raw, encoder_offset
        return False, False, False

    def read_multiloop_angle(self):
        """ Read angle multiplied by made turns

        Returns:
            (float) : motor angle up to 0.01 deg
        """
        ret = self.get_data(READ_MULTILOOP_ANGLE)
        if ret:
            angle = self.hex2int(ret[::-1], signed=True)
            return angle*0.01
        return False

    def read_singleloop_angle(self):
        """ Read angle in term of single turn

        Returns:
            (float) : motor angle up to 0.01 deg
        """
        ret = self.get_data(READ_SINGLELOOP_ANGLE)
        if ret:
            angle = self.hex2int(ret[::-1])
            return angle*0.01
        return False

    def read_pid(self):
        """ Read PID parameters values

        Returns:
            (int) : Angle Kp gain
            (int) : Angle Ki gain
            (int) : Speed Kp gain
            (int) : Speed Ki gain
            (int) : Torque Kp gain
            (int) : Torque Ki gain            
        """
        data = self.get_data(READ_PID_PARAM)
        if data:
            angleKp = self.hex2int(data[0])
            angleKi = self.hex2int(data[1])
            speedKp = self.hex2int(data[2])
            speedKi = self.hex2int(data[3])
            torqueKp = self.hex2int(data[4])
            torqueKi = self.hex2int(data[5])
            return angleKp, angleKi, speedKp, speedKi, torqueKp, torqueKi
        return False, False, False, False, False, False

    def write_pid(self, angleKp, angleKi, speedKp, speedKi, torqueKp, torqueKi, mem='RAM'):
        """ Write PID parameters to memory: RAM or ROM

        Args:
            angleKp (int) : Kp gain value for angle control loop
            angleKi (int) : Ki gain value for angle control loop
            speedKp (int) : Kp gain value for speed control loop
            speedKi (int) : Ki gain value for speed control loop
            torqueKp (int) : Kp gain value for torque control loop
            torqueKi (int) : Ki gain value for torque control loop
            mem (str) : target memory, 'RAM' or 'ROM'
        Returns:
            (int) : 1 - success, 0 - failed, -1 - wrong memory specified

        """
        data = [angleKp, angleKi, speedKp, speedKi, torqueKp, torqueKi]
        if mem == 'RAM':
            return self.send_data(WRITE_PID_PARAM_TO_RAM, data)
        elif mem == 'ROM':
            return self.send_data(WRITE_PID_PARAM_TO_ROM, data)
        else:
            print('Wrong memory specified')
            return -1

    def set_encoder_offset(self, offset=0):
        """ Set encoder offset value

        Args:
            offset (int) : encoder offset value, 0-16383
        Returns:
            (bool) : success
        """
        encoder_offset = bytearray.fromhex(self.int2hex(offset))
        data = [encoder_offset[1], encoder_offset[0]]
        return self.send_data(cmd=WRITE_ENCODER_AS_ZERO, data=data)

    def read_motor_voltage_and_temperature(self):
        """ Read motor STATUS1 - temperature, voltage and errors

        Returns:
            (int) : temperature [C]
            (float) : voltage [V]
            (bool) : low voltage error
            (bool) : over temperature error
        """
        data = self.get_data(READ_MOTOR_STATUS1_AND_ERROR)
        if data:
            temperature = self.hex2int(data[0])
            voltage = round(self.hex2int([data[3], data[2]])*0.1, 2)
            error = self.hex2int(data[6])
            low_voltage_error = 0
            over_temperature_error = 0
            if error == 1:
                low_voltage_error = 1
            elif error == 8:
                over_temperature_error = 1
            elif error == 9:
                low_voltage_error = 1
                over_temperature_error = 1

            return temperature, voltage, low_voltage_error, over_temperature_error
        else:
            return False, False, False, False

    def read_motor_status(self, ret_temperature=False):
        """ Read motor STATUS2 - temperature, torque, speed and encoder values

        Args:
            ret_temperature (bool) : if temperature should be returned as well with other values
        Returns:
            (float) : torque current [A]
            (int) : speed [dps]
            (int) : encoder value
            (int) : temperature [C] if ret_temperature, 0 otherwise
        """
        data = self.get_data(READ_MOTOR_STATUS2)
        if data:
            temperature = self.hex2int(data[0])
            torque = self.hex2int([data[2], data[1]], signed=True) / 64 #* 33/2048
            speed = self.hex2int([data[4], data[3]], signed=True)
            encoder = self.hex2int([data[6], data[5]])
            if not ret_temperature:
                return torque, speed, encoder, 0
            else:
                return torque, speed, encoder, temperature
        else:
            return False, False, False, False

    def read_motor_current(self, ret_temperature=False):
        """ Read motor STATUS3 - temperature and phase current for phases A, B and C

        Args:
            ret_temperature (bool) : if temperature should be returned as well with phase current
        Returns:
            (float) : phase A current [A]
            (float) : phase B current [A]
            (float) : phase C current [A]
            (int) : temperature [C] if ret_temperature, 0 otherwise
        """
        data = self.get_data(READ_MOTOR_STATUS3)
        if data:
            temperature = self.hex2int(data[0])
            iA = self.hex2int([data[2], data[1]]) * 1/64
            iB = self.hex2int(data[4], data[3]) * 1/64
            iC = self.hex2int(data[6], data[5]) * 1/64
            if not ret_temperature:
                return iA, iB, iC, 0
            else:
                return iA, iB, iC, temperature
        else:
            return False, False, False, False

    def clear_error(self):
        """ Clear RMD driver error flag

        Returns:
            (bool) : success
        """
        return self.send_data(cmd=CLEAR_MOTOR_ERROR, clear_response=True)
        
    def get_data(self, cmd):
        """ Receive data from RMD driver based on cmd

        Args:
            cmd (hex) : hex of command
        Returns:
            (bool|list) : False if failed, data list otherwise
        """
        frame = self._create_frame_data(cmd)
        if self._send_command(frame):
            ret = self._handle_command_response()
            return ret
        else:
            return False

    def send_data(self, cmd, data=None, clear_response=True):
        """ Send command with data to RMD driver

        Args:
            cmd (hex) : hex of command
            data (list) : list of hexes as command data
        Returns:
            (bool|list) : False if fail, True if sent and clear_response=True, response otherwise
        """
        frame = self._create_frame_data(cmd, data)
        if self._send_command(frame):
            ret = self._handle_command_response(clear=clear_response)
            return ret
        else:
            return False

    def _handle_command_response(self, clear=False):
        """ Wait for incoming message and handle it properly

        Args:
            clear (bool) : if input buffer should be cleared instead of reading
        Returns:
            (bool|list) : True|False if no data to return, data list otherwise
        """
        ret = []
        if clear:
            self.port.reset_input_buffer()
        while not self.port.in_waiting > 0:
            time.sleep(RMD.RESPONSE_WAIT_TIME_SEC)
        if clear:
            self.port.reset_input_buffer()
            return True
        else:
            ret = self._read_frame()
            if ret:
                return ret
            return False

    def _send_command(self, frame):
        """ Send command frame to RMD driver

        Args:
            frame (bytearray) : RMD frame to send
        Returns:
            (bool) : wheter number of sent bytes equals frame length
        """
        ret = self.port.write(frame)
        if ret == len(frame):
            return True
        else:
            return False

    def _read_frame(self):
        """ Try to read from waiting serial buffer

        Returns:
            One of following:
                (int|list) : 0 if data not received, -1 if not ok, data list otherwise 
        """
        if self.port.in_waiting:
            header = self.port.read(5)
            header_fields = list(header)
            if header_fields[3] > 0x00:
                data = self.port.read(header_fields[3]+1)
            if data:
                received = header + data
            else:
                received = header
            frame_ok, parsed = self._parse_frame(received)

            if frame_ok:
                return parsed
            else:
                return -1
        else:
            return False

    def _create_frame_data(self, cmd, data=None):
        """ Create RMD frame based on passed command hex

        Args:
            cmd (hex) : hex of command
            data (list) : list of hexes as command data
        Returns:
            (bytearray) : RMD frame to send
        """
        frame = bytearray()
        size = 1+1+1+1+1 # head + command + ID + length + check
        data_length = 0
        if data:
            data_length = len(data)
            size += data_length # data size
            size += 1 # check

        head_checksum = (RMD._HEAD_BYTE + cmd + self.ID + data_length) % 256
        if data:
            data_checksum = sum(data) % 256

        frame.append(RMD._HEAD_BYTE)
        frame.append(cmd)
        frame.append(self.ID)
        frame.append(data_length)
        frame.append(head_checksum)
        if data:
            frame.extend(data)
            frame.append(data_checksum)
        return frame

    def _parse_frame(self, frame):
        """ Parse received frame from RS485

        Args:
            frame (bytearray) : received data
        Returns:
            (bool) : wheter received frame was ok (id, checksums)
            (list|None) : received data if any, None otherwise
        """
        fields = list(frame)
        id = fields[2]
        data_length = fields[3]
        head_checksum = fields[4]
        data_received = list()
        if data_length > 0:
            for i in range(data_length):
                data_received.append(frame[5+i])
            data_checksum = fields[4+data_length+1]
        
        if id != self.ID:
            print('Invalid id')
            return False, []

        if head_checksum != sum(fields[0:4]) % 256:
            print(f'Invalid head checksum: {head_checksum} instead of {sum(fields[0:3])}')
            return False, []

        if data_checksum and data_checksum != sum(data_received) % 256:
            print(f'Invalid data checksum: {data_checksum} instead of {sum(data_received)%256} with data {data_received}')
            return False, []

        if data_received:
            return True, data_received
        else:
            return True, []

    @staticmethod
    def int2hex(intnum, signed=False, nbits=0):
        """ Convert integer to hex understable by bytearray

        Args:
            intnum (int) : integer to convert
            signed (bool) : if number is negative
            nbits (int) : for negative number, number of bits to write value to (bytes*4)
        Returns:
            (str) : string of hex value without '0x' prefix
        """
        if not signed:
            hexstr = hex(round(intnum)).replace('0x','')
        else:
            hexstr = hex((round(intnum) + (1 << nbits)) % (1 << nbits))
            hexstr = hexstr.replace('0x','')

        if (len(hexstr) % 2 == 1):
            hexstr = '0' + hexstr
        return hexstr

    @staticmethod
    def hex2int(hexnum, signed=False):
        """ Convert hex string or list of hex strings into integer
        
        Args:
            hexnum (str|list) : hex string or list
            signed (bool) : for converting from negative number
        Returns:
            (int) : integer number
        """
        if not isinstance(hexnum, list):
            hexnum = [hexnum]
        intnum = 0
        bits = len(hexnum)*8
        for i in range(len(hexnum)):
            intnum += hexnum[-i-1] << (8*i)
        if signed and (intnum & (1 << (bits-1))):
            intnum -= 1 << bits
        return intnum

    @staticmethod
    def adjust_len(array, desire_len):
        """ Adjust length of input array to desired length in bytes
        
        Args:
            array (hex int) : input number
            desire_len (int) : desired length of output in bytes
        Returns:
            (hex) : hex adjusted by adding leading zeros
        """
        actual_len = len(array)
        while actual_len < desire_len:
            array.insert(0,0)
            actual_len = len(array)
        return array

if __name__ == '__main__':
    pass
