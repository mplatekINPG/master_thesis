import ctypes

def hex2int(hexnum, signed=False):
    if not signed:
        if isinstance(hexnum, list):
            if isinstance(hexnum[0], str):
                intnum = int(''.join(hexnum), 16)
            elif isinstance(hexnum[0], int):
                intnum = int(''.join(map(str,hexnum)), 16)
        if isinstance(hexnum, str):
            intnum = int(hexnum, 16)
        if isinstance(hexnum, int):
            intnum = int(hexnum)
    else:
        if isinstance(hexnum, list):
            if isinstance(hexnum[0], str):
                intnum = int(''.join(hexnum), 16) - int(hexnum[-1], 16)
            elif isinstance(hexnum[0], int):
                intnum = int(''.join(map(str,hexnum)), 16) - 2**len(hexnum)
    return intnum

# def simple_int(hexnum, signed=False):
#     if not isinstance(hexnum, list):
#         hexnum = [hexnum]
#     res = ctypes.c_uint(0)
#     bits = len(hexnum)*8
#     for i in range(len(hexnum)):
#         res.value += hexnum[-i-1] << (8*i)
#     if signed and (res.value & (1 << (bits-1))):
#         res.value -= 1 << bits
#     # print(bin(res))
#     if signed:
#         return res.value
#     return ctypes.c_int(res.value).value

def simple_int(hexnum, signed=False):
    if not isinstance(hexnum, list):
        hexnum = [hexnum]
    res = 0
    bits = len(hexnum)*8
    for i in range(len(hexnum)):
        res += hexnum[-i-1] << (8*i)
        print(bin(res))
    if signed and (res & (1 << (bits-1))):
        res -= 1 << bits
        print(f'{res} lowered by {1 << (bits)}')
    print(bin(res))
    if signed:
        return res
    return res
    
# print(f'in: [32,68] (0x3268), out: {simple_int([32,68])}')
# print(f'in: 255 (0xFF), out: {simple_int(255)}')
print(f'in: [138, 216] (0x3268), out: {simple_int([138,216])}')
print(f'in: [255, 255, 255, 255, 255, 255, 254, 57] (0xFFFF FFFF FFFF FE57‬), out: {simple_int([255, 255, 255, 255, 255, 255, 254, 57], True)}')

# print(f'in: [32,68], out: {hex2int([32,68])}')
# print(f'in: [255, 255, 255, 255, 255, 255, 255, 156], out: {hex2int([255, 255, 255, 255, 255, 255, 255, 156], True)}')