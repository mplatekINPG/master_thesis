from numpy.lib.utils import info
import wx
import time
from datetime import date
import numpy
import logging

from wx.core import Panel
# import rmd

logging.basicConfig(filename='control_panel-' + date.today().strftime("%Y-%m-%d") + '.log', 
                    format = '%(asctime)s - %(levelname)s : %(message)s',
                    #encoding='utf-8',
                    level=logging.INFO)

def info(msg):
    logging.info(msg)

def error(msg):
    logging.error(msg)

def warning(msg):
    logging.warning(msg)


class Frame(wx.Frame):
    def __init__(self, title):
        wx.Frame.__init__(self, None, title=title, size=(480,360))

        self.motor1 = self.RMDControls(id=0x01)
        #self.motor2 = self.RMDControls(id=0x02)
        #self.motor3 = self.RMDControls(id=0x03)

        self.menu = self.InitMenuBar()
        self.SetMenuBar(self.menu)

        self.status_bar = self.InitStatusBar()
        
        self.box = wx.BoxSizer()
        self.notebook, self.main_panel = self.InitMainPanel()
        self.box.Add(self.notebook, 1, wx.EXPAND)
        #self.general_panel = self.InitGeneralPanel(self.box, size=(450, 50), pos=(5,5))
        #self.panel2 = self.InitPanel(self.box, size=(100, 200), pos=(300,5))

        
        # panel = wx.Panel(self)
        # button = wx.Button(panel, -1, "temp")
        # self.box.Add(button, 0, wx.ALL, 10)

        # self.panel.SetSizer(self.box)
        # self.panel.Layout()

        self.main_panel.SetSizer(self.box)
        #self.Show()

    def InitMenuBar(self):
        menu_bar = wx.MenuBar()
        
        power_menu = wx.Menu()
        restart_button = power_menu.Append(wx.ID_ANY, 'Restart', 'Restart device and application')
        poweroff_button = power_menu.Append(wx.ID_ANY, 'Turn Off', 'Turn off device and application')
        
        self.Bind(wx.EVT_MENU, self.onRestart, restart_button)
        self.Bind(wx.EVT_MENU, self.onTurnOff, poweroff_button)

        about_menu = wx.Menu()
        about_button = about_menu.Append(wx.ID_ANY, 'About', 'Show information about this application')

        self.Bind(wx.EVT_MENU, self.onAbout, about_button)

        menu_bar.Append(power_menu, 'P&ower')
        menu_bar.Append(about_menu, 'About')
        return menu_bar

    def InitStatusBar(self):
        return self.CreateStatusBar()

    def InitMainPanel(self):
        panel = wx.Panel(self)
        nb = wx.Notebook(panel)

        tabControl = self.InitControl(nb)
        tabPID = self.InitPid(nb)
        tabRead = self.InitReading(nb)

        nb.AddPage(tabControl, 'Control')
        nb.AddPage(tabPID, 'PID')
        nb.AddPage(tabRead, 'Status')

        return nb, panel
    
    def InitGeneralPanel(self, box, size=None, pos=None):
        if pos and size:
            panel = wx.Panel(self, pos=pos, size=size)
        elif pos and not size:
            panel = wx.Panel(self, pos=pos)
        elif not pos and size:
            panel = wx.Panel(self, size=size)
        else:
            panel = wx.Panel(self)

        panel.SetBackgroundColour("white")
        
        start_motor_button = wx.Button(panel, wx.ID_ANY, "Start", pos=(10,10))
        stop_motor_button = wx.Button(panel, wx.ID_ANY, "Stop", pos=(70,10))
        shutdown_motor_button = wx.Button(panel, wx.ID_ANY, "Shutdown", pos=(130,10))
        
        box.Add(start_motor_button, 0, wx.ALL, 10)
        box.Add(stop_motor_button, 0, wx.ALL, 10)
        box.Add(shutdown_motor_button, 0, wx.ALL, 10)

        self.Bind(wx.EVT_BUTTON, self.motor1.onStart, start_motor_button)
        return panel

    def InitBox(self):
        box = wx.BoxSizer(wx.VERTICAL)

        return box

    def InitControl(self, parent):
        panel = wx.Panel(parent)
        t = wx.StaticText(self, -1, "Some random text", (20,20))
        return panel

    def InitPid(self, parent):
        panel = wx.Panel(parent)
        t = wx.StaticText(self, -1, "Some random text 2", (20,20))
        return panel

    def InitReading(self, parent):
        panel = wx.Panel(parent)
        t = wx.StaticText(self, -1, "Some random text 3", (20,20))
        return panel

    def onRestart(self, event):
        dialog_box = wx.MessageDialog(self, "Device will restart", 'Restart', wx.OK|wx.CANCEL|wx.ICON_QUESTION)
        result = dialog_box.ShowModal()
        dialog_box.Destroy()
        if result == wx.ID_OK:
            pass 
            # handle restart

    def onTurnOff(self, event):
        dialog_box = wx.MessageDialog(self, "Device will turn off", 'Turn Off', wx.OK|wx.CANCEL|wx.ICON_QUESTION)
        result = dialog_box.ShowModal()
        dialog_box.Destroy()
        if result == wx.ID_OK:
            pass 
            # handle turn off 

    def onAbout(self, event):
        msg = "Helloooo"
        dialog_window = wx.MessageBox(msg, 'Informations')


    class RMDControls():#rmd.RMD):
        def __init__(self, id=0x01, port=0):
            self.id = id
            # super().__init__(id=id, port=port)

        def onStart(self, event):
            pass
            # print(f'Hello from {self.id}')
            # self.motor.start()

if __name__ == '__main__':
    app = wx.App()#redirect=True)
    top = Frame('<<project>>')
    top.Show()
    app.MainLoop()