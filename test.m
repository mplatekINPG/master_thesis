close all; clear all
t = [0:10, 12:25, 27:50, 52:78, 82:100];
missing = [];
last_ok = t(1);

%%
MAX_SAMPLE = 60000;
data = readtable('2021-03-06-focus-s-1.csv');
%data = readtable('2021-02-07-golf-s.csv');
t = data.timestamp + 1; % matlab iterates from 1
cross = 0;
t_long(1) = t(1);
for i=2:1:length(t)
    if (t(i-1)-t(i) > 0.95*MAX_SAMPLE)
        cross = cross + 1;
    end
    t_long(i) = t(i) + cross*(MAX_SAMPLE);
end
t = t_long';

for i=1:length(t)
    dif(i) = t(i)-i;
end
for i=2:length(dif)
    if dif(i)-dif(i-1) > 0
        disp(num2str(i))
    end
end