clear all; close all;
xs = 1:20;
y1 = [1, 4, 4, 4, 4, 5, 5, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4];
y2 = [15, 19, 13, 19, 19, 19, 19, 20, 17, 17, 18, 19, 12, 13, 12, 12, 12, 13, 12, 13];
y3 = [34, 35, 18, 34, 35, 35, 36, 31, 20, 20, 31, 34, 20, 20, 21, 21, 20, 21, 21, 21];

figure()
scatter(xs, y1, 25, [0.8500 0.3250 0.0980], 'filled')
hold on
scatter(xs, y2, 25, [0 0.4470 0.7410], 'filled')
scatter(xs, y3, 25, [0.4660 0.6740 0.1880], 'filled')
line([0 20], [0, 20], 'Color', 'k')
xlabel('Zadana częstotliwość')
ylabel('Otrzymane częstotliwości dominujące')
title('Osiągane częstotliwości [Hz]')
legend('f_1', 'f_2', 'f_3', 'Wart. ocz.', 'Location', 'best')
grid minor
xlim([0, 21])


% figure()
% plot(xs, y1, 'Color', [0.8500 0.3250 0.0980], 'LineWidth', 2)
% hold on
% plot(xs, y2, 'Color', [0 0.4470 0.7410], 'LineWidth', 2)
% plot(xs, y3, 'Color', [0.4660 0.6740 0.1880], 'LineWidth', 2)
% xlabel('Zadana częstotliwość')
% ylabel('Otrzymane częstotliwości dominujące')
% title('Osiągane częstotliwości [Hz]')
% legend('f_1', 'f_2', 'f_3')
% grid minor
% xlim([0, 21])
