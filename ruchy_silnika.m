clear all; close all;

time = 1.7;
samples = 96;
xs = time/samples : time/samples : time;
l0 = 105.93;
b0 = -42.4;
r0 = -14.1;

left = [NaN,
83.43,
NaN,
NaN,
NaN,
NaN,
79.68,
NaN,
75.93,
NaN,
128.43,
NaN,
NaN,
NaN,
NaN,
132.18,
NaN,
135.93,
NaN,
NaN,
83.43,
NaN,
NaN,
NaN,
79.68,
NaN,
75.93,
NaN,
NaN,
128.43,
NaN,
NaN,
NaN,
132.18,
NaN,
135.93,
NaN,
NaN,
83.43,
NaN,
NaN,
NaN,
NaN,
79.68,
NaN,
75.93,
NaN,
NaN,
128.43,
NaN,
NaN,
NaN,
NaN,
132.18,
NaN,
135.93,
83.43,
NaN,
NaN,
NaN,
NaN,
NaN,
NaN,
79.68,
75.93,
128.43,
NaN,
NaN,
NaN,
NaN,
NaN,
NaN,
132.18,
135.93,
NaN,
83.43,
NaN,
NaN,
NaN,
NaN,
NaN,
79.68,
NaN,
75.93,
NaN,
128.43,
NaN,
NaN,
NaN,
NaN,
NaN,
132.18,
NaN,
135.93,
NaN,
NaN];

back = [-58.47, 
    NaN, 
    NaN,
-61.15,
-63.83,
-26.35,
NaN,
NaN,
NaN,
NaN,
NaN,
-23.64,
NaN,
-21,
-58.4,
NaN,
NaN,
NaN,
-61.15,
NaN,
NaN,
-63.83,
NaN,
-26.32,
NaN,
-23.64,
NaN,
NaN,
-20.97,
NaN,
NaN,
-58.47,
NaN,
NaN,
-61.15,
NaN,
-63.83,
NaN,
NaN,
-26.32,
NaN,
NaN,
-23.64,
NaN,
-20.97,
NaN,
-58.47,
NaN,
NaN,
NaN,
-61.15,
NaN,
-63.83,
NaN,
-26.33,
NaN,
NaN,
NaN,
-23.65,
NaN,
-20.97,
NaN,
-58.47,
NaN,
NaN,
NaN,
-61.15,
NaN,
-63.83,
NaN,
-26.32,
NaN,
NaN,
NaN,
-23.65,
NaN,
-20.97,
NaN,
-58.37,
NaN,
NaN,
NaN,
-61.15,
NaN,
-63.83,
NaN,
-26.32,
NaN,
NaN,
NaN,
-23.65,
NaN,
-20.97,
NaN,
-58.47,
NaN];

right = [NaN,
NaN,
-36.6,
NaN,
NaN,
NaN,
NaN,
-40.35,
NaN,
-44.1,
NaN,
NaN,
8.4,
NaN,
NaN,
NaN,
12.15,
NaN,
NaN,
15.9,
NaN,
NaN,
-36.6,
NaN,
NaN,
NaN,
NaN,
-40.35,
NaN,
NaN,
-44.1,
NaN,
8.4,
NaN,
NaN,
NaN,
NaN,
12.15,
NaN,
NaN,
15.9,
-36.6,
NaN,
NaN,
NaN,
NaN,
NaN,
-40.35,
NaN,
-44.1,
NaN,
8.4,
NaN,
NaN,
NaN,
NaN,
NaN,
12.15,
NaN,
15.9,
NaN,
-36.6,
NaN,
NaN,
NaN,
NaN,
NaN,
-40.35,
NaN,
-44.1,
NaN,
8.4,
NaN,
NaN,
NaN,
NaN,
NaN,
12.15,
NaN,
15.9,
-36.6,
NaN,
NaN,
NaN,
NaN,
NaN,
NaN,
-40.35,
-44.1,
8.4,
NaN,
NaN,
NaN,
NaN,
NaN,
12.15];


figure()
subplot(3,1,1)
scatter(xs, back, 'ro')
hold on
line([xs(1), xs(end)], [b0, b0], 'LineStyle', '-.', 'Color', 'r')
grid minor
title('Zadane położenia silnika pracującego z częstotliwością 14 Hz (tylny)')
ylabel('Położenie kątowe [deg]')
legend('Zadane położenie', 'Pozycja "0"', 'Location', 'best')
ylim([-70 -15])

subplot(3,1,2)
scatter(xs, left, 'ko')
hold on
line([xs(1), xs(end)], [l0, l0], 'LineStyle', '-.', 'Color', 'k')
grid minor
title('Zadane położenia silnika pracującego z częstotliwością 10 Hz (lewy)')
ylabel('Położenie kątowe [deg]')
legend('Zadane położenie', 'Pozycja "0"', 'Location', 'best')
ylim([70 140])

subplot(3,1,3)
scatter(xs, right, 'bo')
hold on
line([xs(1), xs(end)], [r0, r0], 'LineStyle', '-.', 'Color', 'b')
grid minor
title('Zadane położenia silnika pracującego z częstotliwością 10 Hz (prawy)')
ylabel('Położenie kątowe [deg]')
legend('Zadane położenie', 'Pozycja "0"', 'Location', 'best')
ylim([-50 20])


figure()
hold on
scatter(xs, (back - min(back))/(max(back) - min(back)), 'ro')
scatter(xs, (left - min(left))/(max(left) - min(left)), 'ko')
scatter(xs, (right - min(right))/(max(right) - min(right)) , 'bo')
grid minor


figure()
sgtitle(['Wartości zadane kąta obrotu silnika w czasie ok. ' num2str(time) ' s'])
subplot(3,1,1)
stairs(xs(isfinite(back)), back(isfinite(back)), 'r:', 'LineWidth', 1.25)
hold on
plot([xs(1), xs(end)], [b0, b0], 'LineStyle', '-', 'Color', 'r', 'LineWidth', 0.1)
grid minor
title('Zadane położenia silnika pracującego z częstotliwością 14 Hz (tylny)')
ylabel('Położenie kątowe [deg]')
xlabel('Czas [s]')
legend('Zadane położenie', 'Pozycja bazowa', 'Location', 'best')
ylim([-70 -15])

subplot(3,1,2)
stairs(xs(isfinite(left)), left(isfinite(left)), 'k:', 'LineWidth', 1.25)
hold on
line([xs(1), xs(end)], [l0, l0], 'LineStyle', '-', 'Color', 'k', 'LineWidth', 0.1)
grid minor
title('Zadane położenia silnika pracującego z częstotliwością 10 Hz (lewy)')
ylabel('Położenie kątowe [deg]')
xlabel('Czas [s]')
legend('Zadane położenie', 'Pozycja bazowa', 'Location', 'best')
ylim([70 140])

subplot(3,1,3)
stairs(xs(isfinite(right)), right(isfinite(right)), 'b:', 'LineWidth', 1.25)
hold on
line([xs(1), xs(end)], [r0, r0], 'LineStyle', '-', 'Color', 'b', 'LineWidth', 0.1)
grid minor
title('Zadane położenia silnika pracującego z częstotliwością 10 Hz (prawy)')
ylabel('Położenie kątowe [deg]')
xlabel('Czas [s]')
legend('Zadane położenie', 'Pozycja bazowa', 'Location', 'best')
ylim([-50 20])
