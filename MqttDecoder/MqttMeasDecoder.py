import paho.mqtt.client as mqtt
import time
import csv
import os
import signal
import argparse

#mqtt_broker = "broker.mqttdashboard.com"
mqtt_broker = "192.168.0.130"
#mqtt_broker = "192.168.0.206"
#mqtt_broker = "192.168.43.130"
ipaddr = "192.168.0.130"#"192.168.43.224"
mqtt_broker = "192.168.43.224"
mqtt_port = 1883
#mqtt_topic = "mgr/meas/bno"
mqtt_topic = "mgr/meas/mma"
mqtt_client = "mgr"

BUFFERSIZE = 5
ELEMENTS_IN_PART = 4

#default_path = "e:/_MGR MATPLAT"
default_path = './'
#csv_file = 'BNO_meas.csv'
csv_file = 'MMA_meas.csv'
default_name = ''
filename = ''
fieldnames = ['timestamp', 'x', 'y', 'z']

def signal_handler(sig, frame):
    print("Ctrl+C detected!")
    print("Disconecting...")
    try: 
        client.disconnect()
    except:
        print("Something went wrong")
        exit(1)
    else:
        print("Done!")
        exit()

def init_csv_file():
    with open(csv_file, mode='w+', newline='') as write_csv:
        writer = csv.DictWriter(write_csv, fieldnames=fieldnames)
        writer.writeheader()
        write_csv.close()

def write_data_to_csv(timestamp, x, y, z):
    with open(csv_file, mode='a', newline='') as write_csv:
        writer = csv.DictWriter(write_csv, fieldnames=fieldnames)
        print(f"Received sample no. {timestamp}")
        writer.writerow({fieldnames[0]: timestamp, fieldnames[1]: x, fieldnames[2]: y, fieldnames[3]: z})
        write_csv.close()

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print(f"Connected to MQTT broker with code '{rc}'")
        else:
            print(f"Failed to connect, return code '{rc}'")

    def on_message(client, userdata, msg):
        data_parts = msg.payload.decode().split(';')
        data_parts = [x for x in data_parts if x]
        for data in data_parts:
            values = data.split(',')
            timestamp = values[0].split('.')[0]
            x = values[1]
            y = values[2]
            z = values[3]
            write_data_to_csv(timestamp, x, y, z)
    
    global client ## make it visible outside
    client= mqtt.Client(mqtt_client)
    client.on_connect = on_connect
    client.on_message = on_message
    try:
        client.connect(mqtt_broker, mqtt_port)
    except:
        print("Connection failed!")
        exit(1)
    return client

if __name__ == '__main__':
    ## Ctrl+C signal handler assign
    signal.signal(signal.SIGINT, signal_handler)

    ## Parse optional argument --path, change to string if needed
    ## Parse optional argument --filename
    parser = argparse.ArgumentParser(description="Decoder of BNO messages sent via MQTT")
    parser.add_argument('--path', 
                        action='store', 
                        nargs='+', 
                        default=default_path, 
                        help="Change working directory") 
    parser.add_argument('-fn', '--filename', 
                        action='store', 
                        nargs='?', 
                        default=default_name, 
                        help="Change name of the output .csv file")
    parser.add_argument('-ip', '--ipaddress', 
                        action='store', 
                        nargs='?', 
                        default=ipaddr, 
                        help="Change MQTT broker's ip")

    path = parser.parse_args().path
    if isinstance(path, str) == False:
        path = ''.join(path)
    filename = parser.parse_args().filename
    mqtt_broker = ''.join(parser.parse_args().ipaddress)

    ## Change working (save) directory
    print(f"Changing working directory to {path} ...")
    try:
        os.chdir(path)
    except:
        print(f"Directory {path} doesn't exist, creating...")
        os.mkdir(path)
        os.chdir(path)
    
    ## Create csv file
    if filename != default_name:
        if ".csv" in filename:
            csv_file = filename
        else:
            csv_file = filename + ".csv"
    print(f"Creating {csv_file} and writing headers...")
    init_csv_file()
    
    ## Connect to broker and subscribe to topic 
    print(f"Connecting to MQTT broker on {mqtt_broker}...")
    client = connect_mqtt()
    time.sleep(5)
    print(f"Subscribing to {mqtt_topic}...")
    client.subscribe(mqtt_topic, 1)
    ## Wait for data
    print(f"Entering loop mode, waiting for data :)")
    while True:
      client.loop()