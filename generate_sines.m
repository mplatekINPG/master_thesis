function [] = generate_sines(freq)
    
clear all;
    if ~exist('freq', 'var')
        freq  = 100; %Hz
    end
    
    no_samples = 5000-1;
    dt = 1/freq;
    StopTime = no_samples * dt;
    t = (0:dt:StopTime)';
    data.timestamp  = 0:1:no_samples;
    
    sine1 = rand(1)*10 + 35; % [25;45] Hz
    sine2 = rand(1)*5 + 15;  % [10;20] Hz
    sine3 = rand(1)*10 + 25; % [15;35] Hz
    
    data.x = sin(2*pi*sine1*t);
    data.x = data.x + sin(2*pi*sine2*t);
    data.x = data.x + sin(2*pi*sine3*t);
    data.x = data.x + rand(1, no_samples+1)';
    
    data.y = sin(2*pi*sine1*t);
    data.y = data.y + sin(2*pi*sine2*t);
    data.y = data.y + sin(2*pi*sine3*t);
    data.y = data.y + rand(1, no_samples+1)';
    
    data.z = sin(2*pi*sine1*t);
    data.z = data.z + sin(2*pi*sine2*t);
    data.z = data.z + sin(2*pi*sine3*t);
    data.z = data.z + rand(1, no_samples+1)';
    data.z = data.z + 9.81;
    
    figure()
    subplot(3,1,1)
    plot(t, data.x)
    subplot(3,1,2)
    plot(t, data.y)
    subplot(3,1,3)
    plot(t, data.z)
    
    data.x = [0; diff(data.x)./diff(t)];
    figure()
    subplot(3,1,1)
    plot(t, data.x)
    subplot(3,1,2)
    plot(t, data.y)
    subplot(3,1,3)
    plot(t, data.z)
    
    data.x = [0; diff(data.x)./diff(t)];
    figure()
    subplot(3,1,1)
    plot(t, data.x)
    subplot(3,1,2)
    plot(t, data.y)
    subplot(3,1,3)
    plot(t, data.z)
    
    save('data.mat', 'data')
end