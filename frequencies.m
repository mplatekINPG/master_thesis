close all; 
clear all;
%% PARAMETERS
% should all Plots be plotted? 0 - no, 1 - yes
PARAMS.plotall = 1;
% add annotations? 0 - no, 1 - yes
PARAMS.annotations = 1;
% maximum number of frequencies to find at the end
PARAMS.max_count = 5;
% maximum sample timestamp in data file
PARAMS.max_sample = 60000;
% sampling frequency
PARAMS.freq = 400; %Hz
% standard gravity
PARAMS.g = 9.80665; %m/s^2
% minium and maximum value available to detect by sensor
PARAMS.range_min = -4*PARAMS.g;
PARAMS.range_max = 4*PARAMS.g;
% static time at each measurement, when car was staying with engine on
PARAMS.time_for_bias = 5; %s
% save figures at the end
PARAMS.save_figures = 1;

%% PROCESSING
%% Loading a file
% data files direcory
Measurement.data_dir = 'pomiary/';

% data file name
% % Measurement.data_file = '2021-03-06-focus-s-2.csv';
% % Measurement.data_file = '2021-03-21-focus-f-merged-big.csv';
% Measurement.data_file = '2021-03-29-jeep-s-1.csv';
% Measurement.data_file = '2021-03-29-jeep-f-1.csv';
% Measurement.data_file = '2021-04-21-focus-s-1.csv';
%  Measurement.data_file = '2021-04-21-focus-f-1.csv';
% Measurement.data_file = '2021-04-18-golf-s-1.csv';
% Measurement.data_file = '2021-04-18-golf-f-1.csv';
% Measurement.data_file = '2021-03-20-astra-s-1.csv';
% Measurement.data_file = '2021-03-20-astra-f-merged.csv';

Measurement.data_file = '2021-08-11-podstawa.csv';
% Measurement.data_file = '2021-08-11-fotelik.csv';


% loading data
Measurement.data = readtable([Measurement.data_dir Measurement.data_file]);

%% Car specification
Car.car_model = [];
if contains(Measurement.data_file, 'focus')
    Car.car_model = 'Ford Focus';
elseif contains(Measurement.data_file, 'astra')
    Car.car_model = 'Opel Astra';
elseif contains(Measurement.data_file, 'golf')
    Car.car_model = 'Volkswagen Golf';
elseif contains(Measurement.data_file, 'jeep')
    Car.car_model = 'Jeep Grand Cheeroke';
elseif contains(Measurement.data_file, 'podstawa')
    Car.car_model = 'Symulator';
elseif contains(Measurement.data_file, 'fotelik')
    Car.car_model = 'Symulator';
end

Car.seat_type = [];
if contains(Measurement.data_file, '-s-')
    Car.seat_type = 'on the seat';
elseif contains(Measurement.data_file, '-f-')
    Car.seat_type = 'in baby seat'; 
elseif contains(Measurement.data_file, 'podstawa')
    Car.seat_type = 'on base'; 
elseif contains(Measurement.data_file, 'fotelik')
    Car.seat_type = 'in baby seat'; 
end
Car.car_model_with_dash = strrep(Car.car_model, ' ', '_');
Car.seat_type_with_dash = strrep(Car.seat_type, ' ', '_');

%% Initial data processing
% assigning values - timestamp and accelerations
Measurement.t = Measurement.data.timestamp + 1; % matlab iterates from 1
if (~issorted(Measurement.t))
    sort(Measurement.t);
end
Measurement.acc.x = Measurement.data.x;
Measurement.acc.y = Measurement.data.y;
Measurement.acc.z = Measurement.data.z;

% taking care of repeating timestamp, defined by PARAMS.max_sample
cross = 0;
t_long(1) = Measurement.t(1);
for i=2:1:length(Measurement.t)
    if (Measurement.t(i-1) - Measurement.t(i) > 0.95*PARAMS.max_sample)
        cross = cross + 1;
    end
    t_long(i) = Measurement.t(i) + cross*(PARAMS.max_sample);
end
Measurement.t = t_long';

% change timestamp to time based on PARAMS.freq
Measurement.time = Measurement.t/PARAMS.freq;

% calculcate difference between each sample
Measurement.dt = Measurement.time(2) - Measurement.time(1);

% print statistics of obtained data by decoder
print_statistics(Measurement.t, PARAMS.freq);

%% Raw acceleration
% plot raw data
if (PARAMS.plotall)
    Plots.raw = figure('units','normalized','outerposition',[0 0 0.75 0.75]);
    plot_them_all(Measurement.time, Measurement.acc.x, Measurement.acc.y, Measurement.acc.z, 'acceleration')
    sgtitle('Raw data, before filtration')
end

disp("##### Before filtration #####")
print_ev(Measurement.acc.x, Measurement.acc.y, Measurement.acc.z);

%% Delete values over range
% filter data out of range
% Measurement.acc.x = filter_in_range(Measurement.acc.x, PARAMS.range_max, PARAMS.range_min, 1);
% Measurement.acc.y = filter_in_range(Measurement.acc.y, PARAMS.range_max, PARAMS.range_min, 1);
% Measurement.acc.z = filter_in_range(Measurement.acc.z, PARAMS.range_max, PARAMS.range_min, 1);
Measurement.acc.x = clear_out_of_range(Measurement.acc.x, PARAMS.range_max, PARAMS.range_min, Measurement.acc.z);
Measurement.acc.y = clear_out_of_range(Measurement.acc.y, PARAMS.range_max, PARAMS.range_min, Measurement.acc.z);
Measurement.acc.z = clear_out_of_range(Measurement.acc.z, PARAMS.range_max, PARAMS.range_min, Measurement.acc.z);
Measurement.time = clear_out_of_range(Measurement.time, PARAMS.range_max, PARAMS.range_min, Measurement.acc.z);

% plot values after initial filtering
if (PARAMS.plotall)
    Plots.range_filtration = figure('units','normalized','outerposition',[0 0 0.75 0.75]);
    plot_them_all(Measurement.time, Measurement.acc.x, Measurement.acc.y, Measurement.acc.z, 'acceleration')
    sgtitle('Data out-of-range changed to mean of neighbours')
end

disp("##### Range filtration #####")
print_ev(Measurement.acc.x, Measurement.acc.y, Measurement.acc.z);

%% Spikes removal
% use median filtration to filter out disturbances (spikes)
Measurement.acc.x = medfilt1(Measurement.acc.x, 5);
Measurement.acc.y = medfilt1(Measurement.acc.y, 5);
Measurement.acc.z = medfilt1(Measurement.acc.z, 7);

% plot filtered data
if (PARAMS.plotall)
    Plots.spikes_filtrations = figure('units','normalized','outerposition',[0 0 0.75 0.75]);
    plot_them_all(Measurement.time, Measurement.acc.x, Measurement.acc.y, Measurement.acc.z, 'acceleration')
    sgtitle('Spikes removed')
end

disp("##### Spikes filtration #####")
print_ev(Measurement.acc.x, Measurement.acc.y, Measurement.acc.z);

%% Bias removal
% calculating mean values of acceleration when car was stationary
Measurement.acc.x_bias = mean(Measurement.acc.x(1 : PARAMS.time_for_bias*PARAMS.freq));
Measurement.acc.y_bias = mean(Measurement.acc.y(1 : PARAMS.time_for_bias*PARAMS.freq));
Measurement.acc.z_bias = mean(Measurement.acc.z(1 : PARAMS.time_for_bias*PARAMS.freq));

Measurement.acc.x = Measurement.acc.x - Measurement.acc.x_bias;
Measurement.acc.y = Measurement.acc.y - Measurement.acc.y_bias;
Measurement.acc.z = Measurement.acc.z - Measurement.acc.z_bias;

% plot filtered data
if (PARAMS.plotall)
    Plots.bias_filtration = figure('units','normalized','outerposition',[0 0 0.75 0.75]);
    plot_them_all(Measurement.time, Measurement.acc.x, Measurement.acc.y, Measurement.acc.z, 'acceleration')
    sgtitle('Filtered data with removed bias')
end

disp("##### Bias filtration #####")
print_ev(Measurement.acc.x, Measurement.acc.y, Measurement.acc.z);

%%
T = length(Measurement.acc.z);
rms = sqrt(1/T * sum(Measurement.acc.z.^2))
rmq = (1/T * sum(Measurement.acc.z.^4))^1/4
NMV = 6*sqrt(quantile(Measurement.acc.z,0.95)^2)

%%
plot_fft(Measurement.acc.z, PARAMS.freq)

%%
function fig = plot_fft(x, fs)
    T = 1/fs;
    L = length(x);
    
    y = fft(x);
    P2 = abs(y/L);
    P1 = P2(1:L/2+1);
    P1(2:end-1) = 2*P1(2:end-1);
    
    maxes = maxk(P1, 2)
    f = fs*(0:(L/2))/L;
    
    fig = figure()
    plot(f, P1)
    grid minor
    xlabel('Frequency [Hz]')
    ylabel('|P1(f)|')
    ylim([0 maxes(2)])
end

function y = clear_out_of_range(x, rmax, rmin, reference)
% FILTER_IN_RANGE Filter out values of x higher than rmax and lower than
%                 rmin in neigh neighbourhood
%   x           - acceleration in x-axis data set
%   y           - acceleration in y-axis data set
%   z           - acceleration in z-axis data set
%   t           - timestamp for data sets
%   rmin, rmax  - boundary values
%   reference   - data set that is a reference to clear values
    y = x(reference > rmin & reference < rmax);
%     y = y(reference > rmin & reference < rmax);
%     z = z(reference > rmin & reference < rmax);
%     t = t(reference > rmin & reference < rmax);

end

function [] = plot_them_all(time, x, y, z, type)
% PLOT_THEM_ALL Plot values of x, y and z arrays in subPlots
%   time    - plot's x values
%   x, y, z - values for certain subPlots
%   type    - define type of the plot (also affects titles and labels)
%             possible values: 'acceleration', 'velocity', 'position'

    if type == "position"
        unit = 'm';
    elseif type == "velocity"
        unit = 'm/s';
    elseif type == "acceleration"
        unit = 'm/s^2';
    end
    
    subplot(3,1,1)
    plot(time, x)
    title(['X axis ' type ' calculation'])
    ylabel([type ' [' unit ']'])
    xlabel('time [s]')
    grid minor
    
    subplot(3,1,2)
    plot(time, y)
    title(['Y axis ' type ' calculation'])
    ylabel([type ' [' unit ']'])
    xlabel('time [s]')
    grid minor
    
    subplot(3,1,3)
    plot(time, z)
    title(['Z axis ' type ' calculation'])
    ylabel([type ' [' unit ']'])
    xlabel('time [s]')
    grid minor
end

function [] = print_statistics(x, freq)
% PRINT STATISTICS Show initial statistics of data set x, based on
%                  sampling frequency freq

    no_missing = max(x) - length(x);
    pr_missing = no_missing/max(x);
    pr_success = (max(x)-no_missing)/max(x);
    disp(['There are ' num2str(no_missing) ' values missing out of ' ...
            num2str(max(x)) ' values sent. '])
    disp(['That is equal to ' num2str(no_missing/freq) ' seconds out of ' ...
            num2str(floor(max(x)/freq/60)) ' min ' num2str(floor(mod(max(x)/freq,60))) ' sec lost.'])
    disp(['It is about ' num2str(pr_missing*100) '% lost data (' ...
            num2str(pr_success*100) '% successfully collected)'])
end

function [] = print_ev(x,y,z)
% PRINT_EV Print mean (estimated values) and variation of samples.
%          Function is prepared to print parameters for acceleration values
%          in x, y and z axes
%   x, y, z - axes 
    
    Ex = mean(x); Vx = var(x);
    Ey = mean(y); Vy = var(y);
    Ez = mean(z); Vz = var(z);
    disp(['X axis acceleration measurement specification: E = ' num2str(Ex) ', V = ' num2str(Vx) ])
    disp(['Y axis acceleration measurement specification: E = ' num2str(Ey) ', V = ' num2str(Vy) ])
    disp(['Z axis acceleration measurement specification: E = ' num2str(Ez) ', V = ' num2str(Vz) ])
end