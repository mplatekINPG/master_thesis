#include <Arduino.h>
#include <PubSubClient.h>
#include <WiFi.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_MMA8451.h>
//#define DEBUG

// tasks 
TaskHandle_t Task1;
TaskHandle_t Task2;
void MMAMeasurement(void * parameter);
void MQTTPublish(void * parameter);
#define BUFFER_SIZE 9
#define ELEMENTS_IN_PART 4
QueueHandle_t queue0;
QueueHandle_t queue1;
void QUEUEsetup();

// MQTT connection
#ifdef DEBUG
char ssid[] = "Nadajnik 5G KRK13 500% mocy";
char pass[] = "3Kotkisierotki";
//char mqtt_server[] = "broker.mqttdashboard.com";
IPAddress mqtt_server(192, 168, 0, 140);
IPAddress mqtt_another_server(192, 168, 0, 130);
#else
char ssid[] = "Platon non-free net";
char pass[] = "qwe1234a";
IPAddress mqtt_server(192, 168, 43, 224);
IPAddress mqtt_another_server(192, 168, 43, 130);
#endif

int status = WL_IDLE_STATUS;
WiFiClient espClient;
PubSubClient client(espClient);
char mqtt_topic[] = "mgr/meas/mma";
void mqtt_reconnect();
void WiFisetup();
void MQTTsetup();

// MMA8451
Adafruit_MMA8451 mma = Adafruit_MMA8451();
#define I2C_SDA 21
#define I2C_SCL 22
#define DATA_RDY 18
sensors_event_t AccData;
void I2Csetup();
void MMAsetup();


void setup() {
  Serial.begin(115200);
  pinMode(DATA_RDY, INPUT);

  Serial.println("I2C initialization....");
  I2Csetup();
  Serial.println("I2C set");
  delay(500);

  Serial.println("MMA initialization....");
  MMAsetup();
  Serial.println("MMA begin success");
  delay(500);
  
  Serial.println("WiFi and MQTT initialization....");
  WiFisetup();
  MQTTsetup();
  Serial.println("WiFi and MQTT initialization success");
  delay(500);

  Serial.println("Queues initialization....");
  QUEUEsetup();
  Serial.println("Queues initialization success");
  delay(500);

  Serial.println("Tasks initialization....");
  xTaskCreatePinnedToCore(MMAMeasurement, "Measuring", 4096, NULL, 1, &Task1, 0);
  delay(500);
  xTaskCreatePinnedToCore(MQTTPublish, "Sending", 4096, NULL, 1, &Task2, 1);
  delay(500);
  Serial.println("Tasks initialization success");
  delay(500);
}

void loop() {
  // task 1 on core 0 : measurment to one of two buffers
  // task 2 on core 1 : mqtt sending
}

void I2Csetup() {
  while (!Wire.begin(I2C_SDA, I2C_SCL, 100000)) {
    Serial.println("I2C failed, trying again");
    delay(2000);
  } 
}

void MMAsetup() {
  while(!mma.begin()) {
    Serial.println("MMA failed to begin");
    delay(2000);
  }
  mma.setRange(MMA8451_RANGE_4_G);
  mma.setDataRate(MMA8451_DATARATE_400_HZ);
  delay(500);
  Serial.print("Defined range is "); 
  Serial.print(2 << mma.getRange());
  Serial.println("G");
  Serial.print("Defined speed is "); 
  Serial.print(800 / (pow(2,(int)mma.getDataRate())));
  Serial.println("Hz");
}

void WiFisetup() {
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    status = WiFi.begin(ssid, pass);
    delay(5000);
  }
  Serial.print("Wifi Connected with IP: ");
  IPAddress ip = WiFi.localIP();
  Serial.println(ip);
}

void MQTTsetup() {
  client.setServer(mqtt_server, 1883);
  mqtt_reconnect();
}

void mqtt_reconnect() {
  int tries = 0;
  while (!client.connected())  {
    Serial.print("Connecting to MQTT broker at "); Serial.println(mqtt_server);
    if(client.connect("espClient")) {
      Serial.println("Connected to MQTT broker");
    }
    else {
      Serial.println("Failed... Trying again in 2 seconds");
      tries += 1;
      delay(2000);
      if (tries > 5) {
        tries = 0;
        mqtt_server = mqtt_another_server;
        client.setServer(mqtt_server, 1883);
      }
    }
  }
}

void QUEUEsetup() {
  queue0 = xQueueCreate(BUFFER_SIZE*ELEMENTS_IN_PART, sizeof(float));
  queue1 = xQueueCreate(BUFFER_SIZE*ELEMENTS_IN_PART, sizeof(float));
  if (queue0 == NULL || queue1 == NULL) {
    Serial.println("Error creating one of queues");
  }
}

void MMAMeasurement(void * parameter) {
  int lastQ = 1;
  float time = 0;
  vTaskDelay(1000 / portTICK_PERIOD_MS);
  for(;;) {
    if (lastQ == 1) {
      for (int i=0; i<BUFFER_SIZE; i++) {
        while (digitalRead(DATA_RDY) != LOW) {
          vTaskDelay(1 / portTICK_PERIOD_MS);
        }
        mma.getEvent(&AccData);
        xQueueSend(queue0, &time, portMAX_DELAY);
        xQueueSend(queue0, &AccData.acceleration.x, portMAX_DELAY);
        xQueueSend(queue0, &AccData.acceleration.y, portMAX_DELAY);
        xQueueSend(queue0, &AccData.acceleration.z, portMAX_DELAY);
        time = int(time)%60000 + 1;
      }
    } 
    else if (lastQ == 0) {
      for (int i=0; i<BUFFER_SIZE; i++) {
        while (digitalRead(DATA_RDY) != LOW) {
          vTaskDelay(1 / portTICK_PERIOD_MS);
        }
        mma.getEvent(&AccData);
        xQueueSend(queue1, &time, portMAX_DELAY);
        xQueueSend(queue1, &AccData.acceleration.x, portMAX_DELAY);
        xQueueSend(queue1, &AccData.acceleration.y, portMAX_DELAY);
        xQueueSend(queue1, &AccData.acceleration.z, portMAX_DELAY);
        time = int(time)%60000 + 1;
      }  
    }
    (lastQ == 0) ? lastQ = 1 : lastQ = 0; 
  }
}

void MQTTPublish(void * parameter) {
  float element;
  vTaskDelay(1000 / portTICK_PERIOD_MS);
  for (;;) {
    if (uxQueueMessagesWaiting(queue0) >= BUFFER_SIZE*ELEMENTS_IN_PART) {
      String msg;
      for (int i=0; i<BUFFER_SIZE*ELEMENTS_IN_PART; i++) {
        xQueueReceive(queue0, &element, portMAX_DELAY);
        msg += element;
        (i%ELEMENTS_IN_PART == ELEMENTS_IN_PART-1) ? msg += ";" : msg += ",";
      }
      char msg_to_send[msg.length()+1];
      msg.toCharArray(msg_to_send, msg.length()+1);
      client.publish(mqtt_topic, msg_to_send);
      Serial.print("Queue0 is: ");
      Serial.println(msg_to_send);
    }
    
    if (uxQueueMessagesWaiting(queue1) >= BUFFER_SIZE*ELEMENTS_IN_PART) {
      String msg;
      for (int i=0; i<BUFFER_SIZE*ELEMENTS_IN_PART; i++) {
        xQueueReceive(queue1, &element, portMAX_DELAY);
        msg += element;
        (i%ELEMENTS_IN_PART == ELEMENTS_IN_PART-1) ? msg += ";" : msg += ",";
      }
      char msg_to_send[msg.length()+1];
      msg.toCharArray(msg_to_send, msg.length()+1);
      client.publish(mqtt_topic, msg_to_send);
      Serial.print("Queue1 is: ");
      Serial.println(msg_to_send);
    }
  }
}