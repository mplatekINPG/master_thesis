close all; 
clear all;
%% PARAMETERS
% should all Plots be plotted? 0 - no, 1 - yes
PARAMS.plotall = 1;
% add annotations? 0 - no, 1 - yes
PARAMS.annotations = 1;
% maximum number of frequencies to find at the end
PARAMS.max_count = 5;
% maximum sample timestamp in data file
PARAMS.max_sample = 60000;
% sampling frequency
PARAMS.freq = 400; %Hz
% standard gravity
PARAMS.g = 9.80665; %m/s^2
% minium and maximum value available to detect by sensor
PARAMS.range_min = -4*PARAMS.g;
PARAMS.range_max = 4*PARAMS.g;
% static time at each measurement, when car was staying with engine on
PARAMS.time_for_bias = 5; %s
% save figures at the end
PARAMS.save_figures = 1;

%% PROCESSING
%% Loading a file
% data files direcory, where measurement files are stored
Measurement.data_dir = 'pomiary/';

% data file name
% please give name of the .csv file with measurements for further
% processing
Measurement.data_file = '2021-03-29-jeep-f-1.csv';
% Measurement.data_file = 'checks/test-07-09-2021-rzecz-500.csv';

% loading data
Measurement.data = readtable([Measurement.data_dir Measurement.data_file]);

%% Car specification
Car.car_model = [];
if contains(Measurement.data_file, 'focus')
    Car.car_model = 'Ford Focus';
elseif contains(Measurement.data_file, 'astra')
    Car.car_model = 'Opel Astra';
elseif contains(Measurement.data_file, 'golf')
    Car.car_model = 'Volkswagen Golf';
elseif contains(Measurement.data_file, 'jeep')
    Car.car_model = 'Jeep Grand Cheeroke';
elseif contains(Measurement.data_file, 'podstawa')
    Car.car_model = 'Simulator';
elseif contains(Measurement.data_file, 'fotelik')
    Car.car_model = 'Simulator';
elseif contains(Measurement.data_file, 'rzecz')
    Car.car_model = 'Simulator';
end

Car.seat_type = [];
if contains(Measurement.data_file, '-s-')
    Car.seat_type = 'on the seat';
elseif contains(Measurement.data_file, '-f-')
    Car.seat_type = 'in baby seat'; 
elseif contains(Measurement.data_file, 'podstawa')
    Car.seat_type = 'on base'; 
elseif contains(Measurement.data_file, 'fotelik')
    Car.seat_type = 'in baby seat'; 
else
    Car.seat_type = 'in baby seat';
end
Car.car_model_with_dash = strrep(Car.car_model, ' ', '_');
Car.seat_type_with_dash = strrep(Car.seat_type, ' ', '_');

%% Initial data processing
% assigning values - timestamp and accelerations
Measurement.t = Measurement.data.timestamp + 1; % matlab iterates from 1
if (~issorted(Measurement.t))
    sort(Measurement.t);
end
Measurement.acc.x = Measurement.data.x;
Measurement.acc.y = Measurement.data.y;
Measurement.acc.z = Measurement.data.z;

% taking care of repeating timestamp, defined by PARAMS.max_sample
cross = 0;
t_long(1) = Measurement.t(1);
for i=2:1:length(Measurement.t)
    if (Measurement.t(i-1) - Measurement.t(i) > 0.95*PARAMS.max_sample)
        cross = cross + 1;
    end
    t_long(i) = Measurement.t(i) + cross*(PARAMS.max_sample);
end
Measurement.t = t_long';

% change timestamp to time based on PARAMS.freq
Measurement.time = Measurement.t/PARAMS.freq;

% calculcate difference between each sample
Measurement.dt = Measurement.time(2) - Measurement.time(1);

% print statistics of obtained data by decoder
print_statistics(Measurement.t, PARAMS.freq);

%% Raw acceleration
% plot raw data
if (PARAMS.plotall)
    Plots.raw = figure('units','normalized','outerposition',[0 0 0.75 0.75]);
    plot_them_all(Measurement.time, Measurement.acc.x, Measurement.acc.y, Measurement.acc.z, 'acceleration')
    sgtitle('Raw data, before filtration')
end

disp("##### Before filtration #####")
print_ev(Measurement.acc.x, Measurement.acc.y, Measurement.acc.z);

%% Delete values over range
% filter data out of range
Measurement.acc.x = clear_out_of_range(Measurement.acc.x, PARAMS.range_max, PARAMS.range_min, Measurement.acc.z);
Measurement.acc.y = clear_out_of_range(Measurement.acc.y, PARAMS.range_max, PARAMS.range_min, Measurement.acc.z);
Measurement.acc.z = clear_out_of_range(Measurement.acc.z, PARAMS.range_max, PARAMS.range_min, Measurement.acc.z);
Measurement.time = clear_out_of_range(Measurement.time, PARAMS.range_max, PARAMS.range_min, Measurement.acc.z);

% plot values after initial filtering
if (PARAMS.plotall)
    Plots.range_filtration = figure('units','normalized','outerposition',[0 0 0.75 0.75]);
    plot_them_all(Measurement.time, Measurement.acc.x, Measurement.acc.y, Measurement.acc.z, 'acceleration')
    sgtitle('Data out-of-range changed to mean of neighbours')
end

disp("##### Range filtration #####")
print_ev(Measurement.acc.x, Measurement.acc.y, Measurement.acc.z);

%% Spikes removal
% use median filtration to filter out disturbances (spikes)
Measurement.acc.x = medfilt1(Measurement.acc.x, 5);
Measurement.acc.y = medfilt1(Measurement.acc.y, 5);
Measurement.acc.z = medfilt1(Measurement.acc.z, 7);

% plot filtered data
if (PARAMS.plotall)
    Plots.spikes_filtrations = figure('units','normalized','outerposition',[0 0 0.75 0.75]);
    plot_them_all(Measurement.time, Measurement.acc.x, Measurement.acc.y, Measurement.acc.z, 'acceleration')
    sgtitle('Spikes removed')
end

disp("##### Spikes filtration #####")
print_ev(Measurement.acc.x, Measurement.acc.y, Measurement.acc.z);

%% Bias removal
% calculating mean values of acceleration when car was stationary
Measurement.acc.x_bias = mean(Measurement.acc.x(1 : PARAMS.time_for_bias*PARAMS.freq));
Measurement.acc.y_bias = mean(Measurement.acc.y(1 : PARAMS.time_for_bias*PARAMS.freq));
Measurement.acc.z_bias = mean(Measurement.acc.z(1 : PARAMS.time_for_bias*PARAMS.freq));

Measurement.acc.x = Measurement.acc.x - Measurement.acc.x_bias;
Measurement.acc.y = Measurement.acc.y - Measurement.acc.y_bias;
Measurement.acc.z = Measurement.acc.z - Measurement.acc.z_bias;

% plot filtered data
if (PARAMS.plotall)
    Plots.bias_filtration = figure('units','normalized','outerposition',[0 0 0.75 0.75]);
    plot_them_all(Measurement.time, Measurement.acc.x, Measurement.acc.y, Measurement.acc.z, 'acceleration')
    sgtitle('Filtered data with removed bias')
end

disp("##### Bias filtration #####")
print_ev(Measurement.acc.x, Measurement.acc.y, Measurement.acc.z);

%% Calculating comfort
T = length(Measurement.acc.z);
T = max(Measurement.time);
Comfort.RMS.value = (1/T * sum(Measurement.acc.z.^2 * Measurement.dt))^(1/2);
Comfort.RMQ.value = (1/T * sum(Measurement.acc.z.^4 * Measurement.dt))^(1/4);

if Comfort.RMS.value < 0.315
    Comfort.RMS.feeling = 'comfortable (komfortowo)';
elseif Comfort.RMS.value >= 0.315 && Comfort.RMS.value < 0.63
    Comfort.RMS.feeling = 'slightly uncomfortable (nieznacznie niekomfortowo)';
elseif Comfort.RMS.value >= 0.5 && Comfort.RMS.value < 1
    Comfort.RMS.feeling = 'quite uncomfortable (do�� niekomfortowo)';
elseif Comfort.RMS.value >= 0.8 && Comfort.RMS.value < 1.6
    Comfort.RMS.feeling = 'uncomfortable (niekomfortowo)';
elseif Comfort.RMS.value >= 1.25 && Comfort.RMS.value < 2.5
    Comfort.RMS.feeling = 'very uncomfortable (bardzo niekomfortowo)';
elseif Comfort.RMS.value >= 2
    Comfort.RMS.feeling = 'extremally uncomfortable (ekstremalnie niekomfortowo)';
end

if Comfort.RMQ.value < 0.315
    Comfort.RMQ.feeling = 'comfortable (komfortowo)';
elseif Comfort.RMQ.value >= 0.315 && Comfort.RMQ.value < 0.63
    Comfort.RMQ.feeling = 'slightly uncomfortable (nieznacznie niekomfortowo)';
elseif Comfort.RMQ.value >= 0.5 && Comfort.RMQ.value < 1
    Comfort.RMQ.feeling = 'quite uncomfortable (do�� niekomfortowo)';
elseif Comfort.RMQ.value >= 0.8 && Comfort.RMQ.value < 1.6
    Comfort.RMQ.feeling = 'uncomfortable (niekomfortowo)';
elseif Comfort.RMQ.value >= 1.25 && Comfort.RMQ.value < 2.5
    Comfort.RMQ.feeling = 'very uncomfortable (bardzo niekomfortowo)';
elseif Comfort.RMQ.value >= 2
    Comfort.RMQ.feeling = 'extremally uncomfortable (ekstremalnie niekomfortowo)';
end

%% Plotting signal in frequency domain

Plots.measurement_data = figure('units','normalized','outerposition',[0 0 0.75 0.75]);
sgtitle(['Acceleration measured in Z axis in ' num2str(Car.car_model) ' ' num2str(Car.seat_type)])
subplot(2,1,1)
plot(Measurement.time, Measurement.acc.z)
title('Time domain')
xlabel('Time [s]')
ylabel('Acceleration [m/s^2]')
grid minor

subplot(2,1,2)
plot_fft(Measurement.acc.z, PARAMS.freq)
title('Frequency domain')
xlabel('Frequency [Hz]')
ylabel('Spectrum [m/s^2]')
grid minor

%% Plotting spectogram
Plots.spectrogram = figure('units','normalized','outerposition',[0 0 0.75 0.75]);
spectrogram(Measurement.acc.z, blackman(256), 64, 4096, PARAMS.freq, 'yaxis');
ax = gca;
ax.Title.String = ['Frequencies of vibrations on z axis in ', Car.car_model, ' ', Car.seat_type];
ax.YTick = 0:10:PARAMS.freq;
ax.YMinorTick = 'on';
ax.YGrid = 'on';
ax.YMinorGrid = 'on';
ax.YLim = [0 100];

%% Calculating values from spectrogram
[Transform.s,Transform.fs, Transform.ts] = spectrogram(Measurement.acc.z, blackman(256), 64, [0:1:200], PARAMS.freq);

% sum values for different frequencies in time
for i=1:length(Transform.fs)
    Frequencies.values(i,:) = abs(Transform.s(i,:)).^2;
    Transform.freq_ticks(i) = Transform.fs(i);
end

%% Calculating histograms for every frequency
% define frequencies' parameters matrix - mean and standard deviation
Frequencies.params = zeros(size(Frequencies.values,1), 2);
for i=1:length(Frequencies.params)
    Frequencies.params(i,1) = mean(Frequencies.values(i,:));
    Frequencies.params(i,2) = var(Frequencies.values(i,:));
    Frequencies.distribution(i,:) = gauss_distribution(Frequencies.values(i,:), ...
                                                     Frequencies.params(i,1), ...
                                                     sqrt(Frequencies.params(i,2)));
end

% plot exemplary histogram
if (PARAMS.plotall)
    row = 5;
    Plots.histogram_example = figure('units','normalized','outerposition',[0 0 0.75 0.75]);
    plot(Frequencies.values(row,:), Frequencies.distribution(row,:), '.');
    title(['Gaussian distribution for row ' num2str(row) ' of frequencies interval'])    
end

% plot histograms for frequencies
Plots.histograms = figure('units','normalized','outerposition',[0 0 0.75 0.75]);
mesh(Frequencies.values/max(max(Frequencies.values)), Transform.freq_ticks, Frequencies.distribution)
title('Relation between power and probability of appearing for different frequencies')
xlabel('Overall power')
ylabel('Frequency [Hz]')
zlabel('Probability')
grid minor
yticks(Transform.freq_ticks(1):10:max(Transform.freq_ticks))
view(70,60)

%% Finding most powerful frequencies
% calculate power over maximum probability
Frequencies.power_over_distribution = Frequencies.params(:,1)./max(Frequencies.distribution')';

% find PARAMS.max_count maximum points
LocalMaxes.TF = islocalmax(Frequencies.power_over_distribution, 'MinSeparation', 5, 'MaxNumExtrema', PARAMS.max_count);

% plot 
Plots.powers_all = figure('units','normalized','outerposition',[0 0 0.75 0.75]);
plot(Transform.freq_ticks, Frequencies.power_over_distribution, 'LineWidth', 2)
title('Average power divived by maximum appearance probability')
xlabel('Frequencies [Hz]')
grid minor
xticks(Transform.freq_ticks(1):10:max(Transform.freq_ticks))
% plot maximum points
hold on
plot(Transform.freq_ticks(LocalMaxes.TF), Frequencies.power_over_distribution(LocalMaxes.TF), 'r*')

if (PARAMS.annotations)
    % adding annotations
    TF_idx = find(LocalMaxes.TF==1,PARAMS.max_count);
    for i=1:length(TF_idx)
        idx = TF_idx(i);
        [xs, ys] = ds2nfu([Transform.freq_ticks(idx), Transform.freq_ticks(idx)+10],...
                          [Frequencies.power_over_distribution(idx), 5*Frequencies.power_over_distribution(idx)]);
        if (xs(2) < 0.2) 
            xs(2) = xs(2) + 0.2;
        end
        if (ys(2) < 0.2)
            ys(2) = ys(2) + 0.2;
        end
        xs(xs > 1) = xs(xs > 1)/max(xs) - 0.2;
        ys(ys > 1) = ys(ys > 1)/max(ys) - 0.2;
        Annotations.(['a' num2str(i)]) = annotation('textarrow', flip(xs), flip(ys), 'String', [num2str(Transform.freq_ticks(idx)) ' Hz']);
    end
end

% closeup
PARAMS.border_frequency = 80;
Frequencies.power_over_distribution_bounded = Frequencies.power_over_distribution(Transform.freq_ticks < PARAMS.border_frequency);
Transform.freq_ticks_bounded = Transform.freq_ticks(Transform.freq_ticks < PARAMS.border_frequency);
% find PARAMS.max_count maximum points
Plots.powers_bounded = figure('units','normalized','outerposition',[0 0 0.75 0.75]);
plot(Transform.freq_ticks_bounded, Frequencies.power_over_distribution_bounded, 'LineWidth', 2)
title(['Average power divived by maximum appearance probability bounded to ' num2str(PARAMS.border_frequency) ' Hz'])
xlabel('Frequencies [Hz]')
grid minor
xticks(Transform.freq_ticks_bounded(1):10:max(Transform.freq_ticks_bounded))
% plot maximum points
hold on
plot(Transform.freq_ticks_bounded(LocalMaxes.TF(1:PARAMS.border_frequency)), Frequencies.power_over_distribution_bounded(LocalMaxes.TF(1:PARAMS.border_frequency)), 'r*')

if (PARAMS.annotations)
    % adding annotations
    TF_idx = find(LocalMaxes.TF==1,PARAMS.max_count);
    for i=1:length(TF_idx)
        idx = TF_idx(i);
        if (idx > PARAMS.border_frequency)
            continue
        end
        [xs, ys] = ds2nfu([Transform.freq_ticks(idx), Transform.freq_ticks(idx)+10],...
                          [Frequencies.power_over_distribution(idx), 5*Frequencies.power_over_distribution(idx)]);
        if (xs(2) < 0.2) 
            xs(2) = xs(2) + 0.2;
        end
        if (ys(2) < 0.2)
            ys(2) = ys(2) + 0.2;
        end
        xs(xs > 1) = xs(xs > 1)/max(xs) - 0.2;
        ys(ys > 1) = ys(ys > 1)/max(ys) - 0.2;
        Annotations.(['a' num2str(i)]) = annotation('textarrow', flip(xs), flip(ys), 'String', [num2str(Transform.freq_ticks(idx)) ' Hz']);
    end
end

%% Final information displayed
LocalMaxes.TF_idx = find(LocalMaxes.TF==1,PARAMS.max_count) - 1;
disp('##### Final informations #####')
for i=1:PARAMS.max_count
   disp(['Significant frequency no.' num2str(i) ' is <strong>around ' num2str(LocalMaxes.TF_idx(i)) ' Hz</strong>']) 
end
disp('In terms of different indices, the ride can be considered as:')
disp([9 '- RMS: ' num2str(Comfort.RMS.feeling) ' (' num2str(Comfort.RMS.value) ' m/s^2)'])
disp([9 '- RMQ: ' num2str(Comfort.RMQ.feeling) ' (' num2str(Comfort.RMQ.value) ' m/s^2)'])

%% Save figures
if (PARAMS.save_figures)
    folder = strcat('plots/', Car.car_model_with_dash, '/', Car.seat_type_with_dash);
    if ~exist(folder, 'dir')
        mkdir(folder)
    end
    extension = '.jpg';
    saveas(Plots.bias_filtration, strcat(folder, '/data', extension));
    saveas(Plots.measurement_data, strcat(folder, '/measurement', extension));
    saveas(Plots.spectrogram, strcat(folder, '/spectrogram', extension));
    saveas(Plots.histograms, strcat(folder, '/histograms', extension));
    saveas(Plots.powers_all, strcat(folder, '/powers', extension));
    saveas(Plots.powers_bounded, strcat(folder, '/powers_bounded', extension));
    disp(['Figures saved in ' folder])
end

%% FUNCTIONS
function y = clear_out_of_range(x, rmax, rmin, reference)
% FILTER_IN_RANGE Filter out values of x higher than rmax and lower than
%                 rmin in neigh neighbourhood
%   x           - acceleration in x-axis data set
%   y           - acceleration in y-axis data set
%   z           - acceleration in z-axis data set
%   t           - timestamp for data sets
%   rmin, rmax  - boundary values
%   reference   - data set that is a reference to clear values
    y = x(reference > rmin & reference < rmax);
%     y = y(reference > rmin & reference < rmax);
%     z = z(reference > rmin & reference < rmax);
%     t = t(reference > rmin & reference < rmax);

end

function y = filter_in_range(x, rmax, rmin, neigh)
% FILTER_IN_RANGE Filter out values of x higher than rmax and lower than
%                 rmin in neigh neighbourhood
%   x           - data set
%   rmin, rmax  - boundary values
%   neigh       - size of neighbourhood

    L = length(x);
    y = x;
    for i=1+neigh:L-neigh
        neighbours = [x(i-neigh):x(i-1), x(i+1):x(i+neigh)];
        if ((x(i) > rmax) || (x(i) < rmin))
            if (mean(neighbours) < rmax && mean(neighbours) > rmin)
                y(i) = mean(neighbours);
            elseif mean(neighbours) >= rmax 
                y(i) = rmax;
            elseif mean(neighbours) <= rmin
                y(i) = rmin;
            end
        end
    end
end

function f = gauss_distribution(x, mu, s)
% GAUSS_DISTRIBUTION Calculate values of gaussian distribution based on
%                     signal, it's mean mu and standard deviation s
%   x     - data set
%   mu    - mean of the values
%   s     - standard deviation of the values (square root of variation)

    p1 = -.5 * ((x - mu)/s) .^ 2;
    p2 = (s * sqrt(2*pi));
    f = exp(p1) ./ p2; 
end

function [] = plot_them_all(time, x, y, z, type)
% PLOT_THEM_ALL Plot values of x, y and z arrays in subPlots
%   time    - plot's x values
%   x, y, z - values for certain subPlots
%   type    - define type of the plot (also affects titles and labels)
%             possible values: 'acceleration', 'velocity', 'position'

    if type == "position"
        unit = 'm';
    elseif type == "velocity"
        unit = 'm/s';
    elseif type == "acceleration"
        unit = 'm/s^2';
    end
    
    subplot(3,1,1)
    plot(time, x)
    title(['X axis ' type ' calculation'])
    ylabel([type ' [' unit ']'])
    xlabel('time [s]')
    grid minor
    
    subplot(3,1,2)
    plot(time, y)
    title(['Y axis ' type ' calculation'])
    ylabel([type ' [' unit ']'])
    xlabel('time [s]')
    grid minor
    
    subplot(3,1,3)
    plot(time, z)
    title(['Z axis ' type ' calculation'])
    ylabel([type ' [' unit ']'])
    xlabel('time [s]')
    grid minor
end

function fig = plot_fft(x, fs)
    T = 1/fs;
    L = length(x);
    
    y = fft(x);
    P2 = abs(y/L);
    P1 = P2(1:L/2+1); 
    P1(2:end-1) = 2*P1(2:end-1);
    f = fs*(0:(L/2))/L;
    plot(f, P1)
    ylim([0, max(P1(P1<max(P1)))*1.1])
end

function [] = print_statistics(x, freq)
% PRINT STATISTICS Show initial statistics of data set x, based on
%                  sampling frequency freq

    no_missing = max(x) - length(x);
    pr_missing = no_missing/max(x);
    pr_success = (max(x)-no_missing)/max(x);
    disp(['There are ' num2str(no_missing) ' values missing out of ' ...
            num2str(max(x)) ' values sent. '])
    disp(['That is equal to ' num2str(no_missing/freq) ' seconds out of ' ...
            num2str(floor(max(x)/freq/60)) ' min ' num2str(floor(mod(max(x)/freq,60))) ' sec lost.'])
    disp(['It is about ' num2str(pr_missing*100) '% lost data (' ...
            num2str(pr_success*100) '% successfully collected)'])
end

function [] = print_ev(x,y,z)
% PRINT_EV Print mean (estimated values) and variation of samples.
%          Function is prepared to print parameters for acceleration values
%          in x, y and z axes
%   x, y, z - axes 
    
    Ex = mean(x); Vx = var(x);
    Ey = mean(y); Vy = var(y);
    Ez = mean(z); Vz = var(z);
    disp(['X axis acceleration measurement specification: E = ' num2str(Ex) ', V = ' num2str(Vx) ])
    disp(['Y axis acceleration measurement specification: E = ' num2str(Ey) ', V = ' num2str(Vy) ])
    disp(['Z axis acceleration measurement specification: E = ' num2str(Ez) ', V = ' num2str(Vz) ])
end